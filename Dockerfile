#-----------------------------------------
# Viewer building
#

FROM node:16-alpine AS builder
WORKDIR /src

# To be able to be docker cache friendly, we first install dependencies
COPY ./viewer/package*.json ./
RUN npm update -g \
	&& npm update -g npm
RUN npm install --legacy-peer-deps

# Build viewer
COPY ./viewer ./
RUN npm run build


#-----------------------------------------
# Server building
#

FROM python:3.9-slim

# Environment variables
ENV DB_URL="postgres://user:pass@host/dbname"
ENV FS_URL="osfs:///data/360app"
ENV MODELS_FS_URL="/data/360models"
ENV SERVER_NAME="localhost:5000"
ENV SECRET_KEY="see_readme"
ENV YOLOV6_PATH="/opt/YOLOv6"
ENV BACKEND_MODE="api"
ENV BLUR_STRATEGY="FAST"
ENV DERIVATES_STRATEGY="ON_DEMAND"
ENV WEBP_METHOD="6"

WORKDIR /opt/360app/server

# Install system dependencies
RUN apt update \
	&& apt install -y git gcc ffmpeg libsm6 libxext6 libpq5 \
 	&& pip install waitress \
	&& git clone --depth 1 --branch 0.1.0 https://github.com/meituan/YOLOv6 /opt/YOLOv6 \
    && pip install torch torchvision --extra-index-url https://download.pytorch.org/whl/cpu \
    && pip install -r /opt/YOLOv6/requirements.txt \
	&& apt remove -y gcc \
 	&& rm -rf /var/lib/apt/lists/* \
	&& mkdir -p /opt/360app/server /opt/360app/viewer /data/360app

# Install Python dependencies
COPY ./server/requirements*.txt ./
RUN pip install -r ./requirements.txt && \
    pip install -r ./requirements-dev.txt && \
    pip install -r ./requirements-blur.txt

# Add server source files
WORKDIR /opt/360app
COPY ./images ./images/
COPY ./docker-entrypoint.sh ./
RUN chmod +x ./docker-entrypoint.sh
COPY ./server ./server/

# Add viewer build result
COPY --from=builder /src/build /opt/360app/viewer/build
COPY ./viewer/demo /opt/360app/viewer/demo
COPY ./viewer/public /opt/360app/viewer/public

# Expose service
EXPOSE 5000
ENTRYPOINT ["./docker-entrypoint.sh"]
