# GeoVisio / Viewer

Viewer is the component allowing display of pictures and sequences on a web page. It is written in JavaScript and compatible with all modern web browsers.

## Dependencies

* NodeJS >= 16

## Setup

This will create a bundle suitable for use within a website, including the default website provided with the GeoVisio server.

```bash
npm install
npm build
```

### Testing

A variety of testing tools is made available:

* `npm start` : launches a dev web server
* `npm run test` : unit testing
* `npm run lint` : syntax checks
* `npm run coverage` : amount of tested code

## Usage

See [developer documentation](USAGE.md) for a complete reference of available methods.

## Thanks

A special thanks to awesome people maintaining code on which we heavily rely:

- [Photo Sphere Viewer](https://github.com/mistic100/Photo-Sphere-Viewer)
- [Maplibre GL JS](https://github.com/maplibre/maplibre-gl-js)
- [JS Library Boilerplate](https://github.com/hodgef/js-library-boilerplate)
