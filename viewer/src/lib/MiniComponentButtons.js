import './css/MiniComponentButtons.css';
import Map from './Map';
import Viewer from './Viewer';
import MinimizeIcon from './img/minimize.svg';
import ExpandIcon from './img/expand.svg';
import PictureIcon from './img/picture.svg';
import MapIcon from './img/map.svg';

/**
 * Mini component buttons is a helper to create top-right corner buttons on reduced version of map/viewer.
 * It offers a same design style for both, and handle events.
 *
 * @param {Viewer|Map} parent The parent container
 * @private
 */
export default class MiniComponentButtons {
	constructor(parent) {
		this._parentName = parent instanceof Map ? "map" : "psv";
		this._parentLabel = this._parentName === "map" ? "map" : "picture";
		this._parent = parent;
		this._createButtonsDOM();
	}

	/**
	 * Initializes the DOM
	 *
	 * @private
	 */
	_createButtonsDOM() {
		// Button in bottom left corner (mini widget hidden)
		this.badgeBtn = document.createElement("button");
		this.badgeBtn.classList.add("gvs-mini-buttons-badge");
		const badgeImg = document.createElement("img");
		badgeImg.src = this._parentName === "map" ? MapIcon : PictureIcon;
		badgeImg.alt = "Expand";
		badgeImg.title = `Show the ${this._parentLabel}`;
		this.badgeBtn.appendChild(badgeImg);
		this.badgeBtn.addEventListener("click", () => {
			this.badgeBtn.classList.remove("gvs-visible");
			this._parent._maximize();
		});
		(this._parentName === "map" ? this._parent.psv.parent : this._parent.parent).appendChild(this.badgeBtn);

		// Buttons in map/viewer top right corner (mini widget visible)
		this.container = document.createElement("div");
		this.container.classList.add("gvs-mini-buttons");

		const btnMinimize = document.createElement("button");
		const btnMinimizeIcon = document.createElement("img");
		btnMinimizeIcon.src = MinimizeIcon;
		btnMinimizeIcon.alt = "Minimize";
		btnMinimizeIcon.title = `Hide the ${this._parentLabel}, you can show it again using button in bottom left corner`;
		btnMinimize.addEventListener("click", () => {
			this._parent._minimize();
			this.badgeBtn.classList.add("gvs-visible");
		});
		btnMinimize.appendChild(btnMinimizeIcon);
		this.container.appendChild(btnMinimize);

		const btnExpand = document.createElement("button");
		const btnExpandIcon = document.createElement("img");
		btnExpandIcon.src = ExpandIcon;
		btnExpandIcon.alt = "Expand";
		btnExpandIcon.title = `Make the ${this._parentLabel} appear in full page`;
		btnExpand.appendChild(btnExpandIcon);
		btnExpand.addEventListener("click", () => {
			if(this._parentName === "map") { this._parent.psv.setMapWide(true); }
			else { this._parent.setMapWide(false); }
		});
		this.container.appendChild(btnExpand);

		this._parent.container.appendChild(this.container);
	}
}
