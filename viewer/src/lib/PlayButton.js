import { AbstractButton, registerButton, DEFAULTS } from 'photo-sphere-viewer';

/**
 * @summary Navigation bar play picture in sequence button class
 * @augments {external:photo-sphere-viewer.buttons.AbstractButton}
 * @memberof {external:photo-sphere-viewer.buttons}
 * @private
 */
class PlayButton extends AbstractButton {

  static id = 'sequence-play';
  static icon = '<svg version="1.1" viewBox="0 0 100 100" xmlns="http://www.w3.org/2000/svg" xmlns:svg="http://www.w3.org/2000/svg"> <path d="m 78.839122,44.639674 -50.40778,-33.54823 c -4.2989,-2.86606 -10.11502,0.25309 -10.11502,5.3948 v 67.01458 c 0,5.22643 5.81612,8.26069 10.11502,5.39481 l 50.40778,-33.54823 c 3.79286,-2.44456 3.79286,-8.17667 -7.1e-4,-10.70542 z" fill="currentColor" /> </svg> ';
  static iconActive = '<svg version="1.1" viewBox="0 0 100 100" xmlns="http://www.w3.org/2000/svg" xmlns:svg="http://www.w3.org/2000/svg"> <path d="m 11.057615,10 v 80 h 31.72461 V 10 Z m 46.15821,0 v 80 h 31.72656 V 10 Z" fill="currentColor" /></svg>';

  /**
   * @param {external:photo-sphere-viewer.components.Navbar} navbar The navigation bar
   */
  constructor(navbar) {
    super(navbar, 'psv-button--hover-scale psv-sequence-button');

    this.psv.on("sequence-playing", this);
    this.psv.on("sequence-stopped", this);
  }

  /**
   * @override
   */
  destroy() {
    this.psv.off("sequence-playing", this);
    this.psv.off("sequence-stopped", this);
    super.destroy();
  }

  /**
   * Handle events
   *
   * @param {Event} e Event metadata
   * @private
   */
  handleEvent(e) {
    /* eslint-disable */
    switch (e.type) {
      // @formatter:off
      case "sequence-playing": this.toggleActive(true); break;
      case "sequence-stopped": this.toggleActive(false); break;
      // @formatter:on
    }
    /* eslint-enable */
  }

  /**
   * @override
   * @description Goes to play picture in sequence
   */
  onClick() {
    if(this.psv.isSequencePlaying()) {
      this.toggleActive(false);
      this.psv.stopSequence();
    }
    else {
      this.toggleActive(true);
      this.psv.playSequence();
    }
  }

}

DEFAULTS.lang[PlayButton.id] = 'Play/pause navigation';
registerButton(PlayButton, 'caption:left');
export default PlayButton;
