import { AbstractButton, registerButton, DEFAULTS } from 'photo-sphere-viewer';

/**
 * @summary Navigation bar next picture in sequence button class
 * @augments {external:photo-sphere-viewer.buttons.AbstractButton}
 * @memberof {external:photo-sphere-viewer.buttons}
 * @private
 */
class NextButton extends AbstractButton {

  static id = 'sequence-next';
  static icon = '<svg viewBox="0 0 100 100" version="1.1" id="svg4" xmlns="http://www.w3.org/2000/svg" xmlns:svg="http://www.w3.org/2000/svg"> <path d="M 66.176728,53.93303 41.721163,78.388595 c -2.148533,2.14854 -5.68704,2.14854 -7.835573,0 -2.14854,-2.148533 -2.14854,-5.68704 0,-7.835572 L 54.359518,50.015045 33.822875,29.478403 c -2.14854,-2.14854 -2.14854,-5.68704 0,-7.835581 C 34.897115,20.568582 36.350225,20 37.74086,20 c 1.390103,0 2.843722,0.56865 3.917993,1.642822 l 24.518272,24.455565 c 2.14854,2.14854 2.14854,5.68704 0,7.83558 z" fill="currentColor" /></svg>';

  /**
   * @param {external:photo-sphere-viewer.components.Navbar} navbar The navigation bar
   */
  constructor(navbar) {
    super(navbar, 'psv-button--hover-scale psv-sequence-button');
  }

  /**
   * @override
   * @description Goes to next picture in sequence
   */
  onClick() {
    try {
      this.psv.goToNextPicture();
    } catch(e) {
      console.warn(e);
      this.disable();
    }
  }

}

DEFAULTS.lang[NextButton.id] = 'Next picture in sequence';
registerButton(NextButton, 'caption:left');
export default NextButton;
