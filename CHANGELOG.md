# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

__About upgrading from versions <= 1.3.1__ : many changes have been done on storage and settings during pictures import, to avoid issues you may do a full re-import of your pictures and sequences. This can be done with following command (to adapt according to your setup):

```bash
cd server/
FLASK_APP="src" flask cleanup
FLASK_APP="src" flask process-sequences
```

### Added
- Home and viewer pages can be changed using `MAIN_PAGE` and `VIEWER_PAGE` settings (thanks to Nick Whitelegg)
- Docker compose file for local development (in complement of existing file which uses pre-built Docker image)
- Explicitly document that database should be in UTF-8 encoding (to avoid [binary string issues with Psycopg](https://www.psycopg.org/psycopg3/docs/basic/adapt.html#strings-adaptation))
- Server tests can be run through Docker
- API can serve pictures in both JPEG and WebP formats
- Viewer now supports WebP assets, and are searched for in priority
- Mock images and sequences can be generated for testing with `fill-mock-data` server command (thanks to Antoine Desbordes)
- Viewer map updates automatically URL hash part with a `map` string
- API map tiles offers a `sequences` layer for display sequences paths

### Changed
- Derivates picture files are now by default generated on-demand on first API request. Pre-processing of derivates (old method) can be enabled using `DERIVATES_STRATEGY=PREPROCESS` setting when calling `process-sequences` command.
- Internal storage format for pictures is now WebP, offering same quality with reduce disk usage.
- If not set, `SERVER_NAME` defaults to `localhost.localdomain:5000`
- Reduced size of Docker image by limiting YOLOv6 repository download and removing unused torchaudio dependency
- Server dependencies are now separated in 3 pip requirements files for faster CI: `requirements.txt`, `requirements-dev.txt` and `requirements-blur.txt`
- During sequences processing, ready pictures can be shown and queried even if whole sequences is not ready yet
- Improved CLI commands documentation (which appears using `FLASK_APP="src" flask --help`)
- Heading in pictures metadata is now optional, and is set relatively to sequence movement path if missing
- New CLI command `set-sequences-heading` allows user to manually change heading values
- Viewer supports STAC items not having `view:azimuth` property defined

### Fixed
- Some sequences names were bytestring instead of string, causing some STAC API calls to fail
- YOLOv6 release number is now fixed in code to avoid issues in downloaded models
- Docker-compose files explicitly wait for PostgreSQL database to be ready to prevent random failures
- With `COMPROMISE` blur strategies, image not needing blurring failed
- URL to API documentation written without trailing `/` was not correctly handled

### Removed
- No progressive JPEG is used anymore for classic (non-360°) HD pictures.


## [1.3.1] - 2022-08-03

### Added
- A cleaner progress bar (tqdm) is used for progress of sequences processing
- Picture heading is also read from `PoseHeadingDegrees` XMP EXIF tag

### Changed
- Pictures derivates folder is renamed from `gvs_derivates` to `geovisio_derivates` for better readability
- Sequences folder can skip processing if their name starts with either `ignore_`, `gvs_` or `geovisio_`
- Status of pictures and sequences is now visible in real-time in database (instead of one transaction commited at the end of single sequence processing)

### Fixed
- Add version in docker-compose file for better compatibility

## [1.3.0] - 2022-07-20

### Added
- Support of flat / non-360° pictures in viewer and server
- List of contributors and special thanks in readme
- Introduced changelog file (the one you're reading 😁)
- Allow direct access to MapLibre GL map object in viewer using `getMap`
- Allow passing all MapLibre GL map settings through viewer using `options.map` object

### Changed
- Pictures blurring now offers several strategies (`BLUR_STRATEGY` setting) and better performance (many thanks to Albin Calais)
- Viewer has a wider zoom range
- Separate stages for building viewer and server in Dockerfile (thanks to Pascal Rhod)

### Fixed
- Test pictures had some corrupted EXIF tags (related to [JOSM issue](https://josm.openstreetmap.de/ticket/22211))


## [1.2.0] - 2022-06-07

### Added
- A demonstration page is available, showing viewer and code examples
- A map is optionally available in viewer to find pictures more easily
- New API route for offering vector tiles (for map) : `/api/map/<z>/<x>/<y>.mvt`
- GeoVisio now has a logo

### Changed
- Improved Dockerfile :
  - Both server and viewer are embed
  - Add list of available environment variables
  - Remove need for a config file
  - A Docker compose file is offered for a ready-to-use GeoVisio with database container
- Server processing for sequences pre-render all derivates versions of pictures to limit I/O with remote filesystems
- Viewer displays a default picture before a real picture is loaded
- Documentation is more complete

### Fixed
- Reading of negative lat/lon coordinates from EXIF tags


## [1.1.0] - 2022-05-09

### Added
- Support of [STAC API scheme](https://github.com/radiantearth/stac-api-spec) for both server and viewer
- New environment variables for database to allow set separately hostname, port, username... : `DB_PORT`, `DB_HOST`, `DB_USERNAME`, `DB_PASSWORD`, `DB_NAME`

### Changed
- All API routes are prefixed with `/api`

### Removed
- `/sequences` API routes, as they are replaced by STAC compliant routes named `/collections`
- Some `/pictures` API routes, as they are replaced by STAC compliant routes named `/collections/<id>/items`


## [1.0.0] - 2022-03-22

### Added
- Server scripts for processing 360° pictures and loading into database
- Support of various filesystems (hard disk, FTP, S3 Bucket...) using PyFilesystem
- API offering sequences, pictures (original, thumbnail and tiled) and various metadata
- Blurring of people, cars, trucks, bus, bicycles on pictures
- Viewer based on Photo Sphere Viewer automatically calling API to search and retrieve pictures
- Dockerfile for easy server setup


[Unreleased]: https://gitlab.com/PanierAvide/geovisio/-/compare/1.3.1...develop
[1.3.1]: https://gitlab.com/PanierAvide/geovisio/-/compare/1.3.0...1.3.1
[1.3.0]: https://gitlab.com/PanierAvide/geovisio/-/compare/1.2.0...1.3.0
[1.2.0]: https://gitlab.com/PanierAvide/geovisio/-/compare/1.1.0...1.2.0
[1.1.0]: https://gitlab.com/PanierAvide/geovisio/-/compare/1.0.0...1.1.0
[1.0.0]: https://gitlab.com/PanierAvide/geovisio/-/commits/1.0.0

