# Develop

You want to work on GeoVisio and offer bug fixes or new features ? That's awesome ! 🤩

Here are some inputs about working with GeoVisio code. If something seems missing or incomplete, don't hesitate to contact us by [email](panieravide@riseup.net) or using [an issue](https://gitlab.com/PanierAvide/geovisio/-/issues). We really want GeoVisio to be a collaborative project, so everyone is welcome (see our [code of conduct](CODE_OF_CONDUCT.md)).

## Install

### With Docker

```bash
# Run API using local code
docker-compose -f docker-compose-dev.yml up --build

# Launch processing of your pictures
docker-compose -f docker-compose-dev.yml run --rm backend process-sequences
```

### Manual install

Check [install doc](./server/README.md#setup) for details.

## Testing

Tests are available in both [server](./server/README.md#testing) and [viewer](./viewer/README.md#testing). If you're working on bug fixes or new features, please make sure to add appropriate tests to keep GeoVisio level of quality.

Note that tests can be run using Docker with following commands:

```bash
# All tests (including heavy ones)
docker-compose -f docker-compose-dev.yml run --rm -e TEST_DB_URL="postgres://gvs:gvspwd@db/geovisio" backend test

# Smaller tests only
docker-compose -f docker-compose-dev.yml run --rm -e TEST_DB_URL="postgres://gvs:gvspwd@db/geovisio" backend test-ci
```

## Make a release

```bash
git checkout develop

cd server/
vim setup.cfg	# Change metadata.version

cd ../viewer/
vim package.json	# Change version
npm run doc

cd ../
vim CHANGELOG.md	# Replace unreleased to version number

git add *
git commit -m "Release x.x.x"
git tag -a x.x.x -m "Release x.x.x"
git push origin develop
git checkout main
git merge develop
git push origin main --tags
```
