# GeoVisio

![GeoVisio logo](./images/logo_full.png)

GeoVisio is a complete solution for storing and __serving your own geolocated pictures__ (like [StreetView](https://www.google.com/streetview/) / [Mapillary](https://mapillary.com/)).

Give it a try at [geovisio.fr](https://geovisio.fr/viewer) !

![Picture viewer screenshot](./images/screenshot.jpg)

## Features

* A __web viewer__ for pictures (including 360°)
  * Move, zoom, drag pictures
  * Travel between pictures in sequences
  * Find pictures by location with a mini-map
* A __web API__ to search / query the pictures collection
  * Search pictures by ID, date, location
  * Compatible with [SpatioTemporal Asset Catalog](https://stacspec.org/) and [OGC WFS 3](https://github.com/opengeospatial/WFS_FES) specifications
* An easy-to-use __backend__
  * Reads through your pictures collection and creates metadata in database
  * Generates automatically thumbnail, small and tiled versions of your pictures
  * Offers blurring of people and vehicle to respect privacy laws
  * Serves a default website and viewer


## Usage

### With Docker

The [Docker](https://docs.docker.com/get-docker/) deployment offers both server and viewer in a single image, and is the simplest way to get a GeoVisio up and running.

```bash
# Create the "pictures" folder
mkdir ./pictures

# Add your sequences in pictures folder
# Each sequence should be a folder containing JPEG images
mv /my/first/sequence ./pictures/first_sequence
mv /my/other/sequence ./pictures/sequence_two
...

# Start GeoVisio API
docker-compose up

# Launch processing of your pictures
docker-compose run --rm backend process-sequences
```

Then, you can access to your instance at [localhost:5000](http://localhost:5000).

#### Database schema migrations for already running instance 

For a one shot deployment there is nothing to do, but when updating a running Geovisio instance, you might need to update the database schema.

To do this, you can just run

```bash
docker-compose run --rm backend db-upgrade
```

### Manual install

Complete installation and usage procedure is described for each component of GeoVisio:

- [Server](./server/), the component handling hosting of pictures. It analyzes and serves your pictures through an API.
- [Viewer](./viewer/), the component allowing display of pictures and sequences on a web page. It is written in JavaScript and compatible with all modern web browsers

## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate. See also [develop doc](DEVELOP.md) for more information.

## Special thanks

![Sponsors](./images/sponsors.png)

GeoVisio was made possible thanks to a group of ✨ __amazing__ people ✨ :

- __[GéoVélo](https://geovelo.fr/)__ team, for 💶 funding initial development and for 🔍 testing/improving software
- __[Carto Cité](https://cartocite.fr/)__ team (in particular Antoine Riche), for 💶 funding improvements on viewer (map browser, flat pictures support)
- __Stéphane Péneau__, for his work on 📷 picture blurring and sharing his 🧙 wisdom on pictures processing
- __Albin Calais__ (mentored by Cyrille Giquello), for implementing 🎚️ multiple picture blurring strategies
- __[Damien Sorel](https://www.strangeplanet.fr/)__, for developing the 📷 amazing [Photo Sphere Viewer](https://photo-sphere-viewer.js.org/) on which GeoVisio viewer heavily depends, and making it evolve to better fit our needs
- __Antoine Desbordes__, for adding a simple way to test Geovisio without having many 📷 pictures available
- __Pascal Rhod__, for improving 🐋 Docker configuration file
- __Nick Whitelegg__, for working on the convergence of GeoVisio and [OpenTrailView](https://opentrailview.org/)
- __[Adrien Pavie](https://pavie.info/)__, for ⚙️ initial development of GeoVisio
- And you all ✨ __GeoVisio users__ for making this project useful !

## License

Copyright (c) Adrien Pavie 2022, [released under MIT license](./LICENSE).
