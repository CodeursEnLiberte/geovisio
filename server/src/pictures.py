import psycopg
from psycopg.rows import dict_row
import re
import io
import os
from fs import open_fs
import math
import time
from concurrent.futures import ThreadPoolExecutor, as_completed
from itertools import repeat
from flask import Blueprint, current_app, request, send_from_directory, send_file
from PIL import Image
from . import errors

bp = Blueprint('pictures', __name__, url_prefix='/api/pictures')


def getPictureSizing(picture):
	"""Calculates image dimensions (width, height, amount of columns and rows for tiles)

	Parameters
	----------
	picture : PIL.Image
		Picture

	Returns
	-------
	dict
		{ width, height, cols, rows }
	"""
	tileSize = getTileSize(picture.size)
	return { "width": picture.size[0], "height": picture.size[1], "cols": tileSize[0], "rows": tileSize[1] }


def getDerivatesPath(pictureId):
	"""Get the path to GeoVisio picture derivates version as a string

	Parameters
	----------
	pictureId : str
		The ID of picture

	Returns
	-------
	str
		The path to picture derivates
	"""
	return f"{current_app.config['PIC_DERIVATES_DIR']}/{str(pictureId)[0:2]}/{pictureId}"


def checkPictureStatus(fs, pictureId):
	"""Checks if picture exists in database, is ready to serve, and retrieves its metadata

	Parameters
	----------
	fs : fs.base.FS
		Filesystem to look through
	pictureId : str
		The ID of picture

	Returns
	-------
	dict
		Picture metadata extracted from database
	"""

	# Check picture availability + status
	with psycopg.connect(current_app.config['DB_URL'], row_factory=dict_row) as db:
		picMetadata = db.execute("""
			SELECT
				status, file_path,
				(metadata->>'cols')::int AS cols,
				(metadata->>'rows')::int AS rows,
				metadata->>'type' AS type
			FROM pictures WHERE id = %s
		""", [pictureId]).fetchone()

		if picMetadata is not None:
			if picMetadata['status'] == "ready":
				# Check derivates availability
				if areDerivatesAvailable(fs, pictureId, picMetadata['file_path'], picMetadata['type']):
					return picMetadata
				else:
					picDerivates = getDerivatesPath(pictureId)

					# Try to create derivates folder if it doesn't exist yet
					fs.makedirs(picDerivates, recreate=True)

					picture = Image.open(fs.openbin("/"+picMetadata['file_path']))

					# Check if a blur mask exists, and apply to original image
					blurMaskFile = picDerivates + "/blur_mask.png"
					if fs.isfile(blurMaskFile):
						blurMask = Image.open(fs.openbin(blurMaskFile))
						picture = createBlurredHDPicture(fs, picture, blurMask, picDerivates + "/blurred.webp", current_app.config['WEBP_METHOD'])

					# Force generation of derivates
					if generatePictureDerivates(fs, picture, getPictureSizing(picture), picDerivates, picMetadata['type'], current_app.config['WEBP_METHOD']):
						return picMetadata
					else:
						raise errors.InvalidAPIUsage("Picture derivates file are not available", status_code=500)
			else:
				raise errors.InvalidAPIUsage("Picture is not available (either hidden by admin or processing)", status_code=403)
		else:
			raise errors.InvalidAPIUsage("Picture can't be found, you may check its ID", status_code=404)

	raise errors.InvalidAPIUsage("Failed to connect to database", status_code=500)


def areDerivatesAvailable(fs, pictureId, pictureFilepath, pictureType):
	"""Checks if picture derivates files are ready to serve

	Parameters
	----------
	fs : fs.base.FS
		Filesystem to look through
	pictureId : str
		The ID of picture
	pictureFilepath : str
		The file path to original picture
	pictureType : str
		The picture type (flat, equirectangular)

	Returns
	-------
	bool
		True if all derivates files are available
	"""

	path = getDerivatesPath(pictureId)

	# Check if HD picture is available (either progressive, blurred or basic)
	if not (fs.isfile(path + "/blurred.webp") or fs.isfile("/" + pictureFilepath)):
		return False

	# Check if SD picture + thumbnail are available
	if not (fs.isfile(path + "/sd.webp") and fs.isfile(path + "/thumb.webp")):
		return False

	# Check if tiles are available
	if pictureType == "equirectangular" and not (fs.isdir(path + "/tiles") and len(fs.listdir(path + "/tiles")) >= 2):
		return False

	return True


def generatePictureDerivates(fs, picture, sizing, outputFolder, type = "equirectangular", webpMethod=6):
	"""Creates all derivated version of a picture (thumbnail, small, tiled)

	Parameters
	----------
	fs : fs.base.FS
		Filesystem to look through
	picture : PIL.Image
		Picture file
	sizing : dict
		Picture dimensions (width, height, cols, rows)
	outputFolder : str
		Path to output folder (relative to instance root)
	type : str (optional)
		Type of picture (flat, equirectangular (default))

	Returns
	-------
	bool
		True if worked
	"""

	# Thumbnail + fixed-with versions
	createThumbPicture(fs, picture, outputFolder + "/thumb.webp", type, webpMethod)
	createSDPicture(fs, picture, outputFolder + "/sd.webp", webpMethod)

	# Tiles
	if type == "equirectangular":
		tileFolder = outputFolder + "/tiles"
		fs.makedir(tileFolder, recreate=True)
		createTiledPicture(fs, picture, tileFolder, sizing["cols"], sizing["rows"], webpMethod)

	return True


def createBlurredHDPicture(fs, picture, blurMask, outputFilename, webpMethod=6):
	"""Create the blurred version of a picture using a blurMask

	Parameters
	----------
	fs : fs.base.FS
		Filesystem to look through
	picture : PIL.Image
		Input image
	blurMask : PIL.Image
		Blur mask, as 2-colors image (white = blurred part)
	outputFilename : str
		Path to output file (relative to instance root)

	Returns
	-------
	PIL.Image
		The blurred version of the image
	"""

	from src.blur.blur import blurPicture
	picture = blurPicture(picture, blurMask)

	blurredPicBytes = io.BytesIO()
	picture.save(blurredPicBytes, format="webp", method=webpMethod, quality=80)
	fs.writebytes(outputFilename, blurredPicBytes.getvalue())

	return picture


def checkFormatParam(format):
	"""Verify that user asks for a valid image format"""

	valid = ['jpg', 'webp']
	if format not in valid:
		raise errors.InvalidAPIUsage("Invalid '"+format+"' format for image, only the following formats are available: "+', '.join(valid), status_code=404)


def sendInFormat(picture, picFormat, httpFormat):
	"""Send picture file in queried format"""

	httpFormat = "jpeg" if httpFormat == "jpg" else httpFormat
	picFormat = "jpeg" if picFormat == "jpg" else picFormat

	if picFormat == httpFormat:
		return send_file(picture, mimetype='image/'+httpFormat)
	else:
		imgio = io.BytesIO()
		Image.open(picture).save(imgio, format=httpFormat, quality=90)
		imgio.seek(0)
		return send_file(imgio, mimetype='image/'+httpFormat)


@bp.route('/<uuid:pictureId>/hd.<format>')
def getPictureHD(pictureId, format):
	"""Get picture image (high-definition)
	---
		parameters:
			- name: pictureId
			  in: path
			  type: string
			  description: ID of picture to retrieve
			- name: format
			  in: path
			  type: string
			  description: Wanted format for output image (either jpg or webp)
		responses:
			200:
				description: High-definition
				content:
					image/jpeg:
						schema:
							type: string
							format: binary
					image/webp:
						schema:
							type: string
							format: binary
	"""

	checkFormatParam(format)

	with open_fs(current_app.config['FS_URL']) as fs:
		picPath = checkPictureStatus(fs, pictureId)['file_path']
		blurPicPath = getDerivatesPath(pictureId) + "/blurred.webp"
		picture = None
		picFormat = None

		try:
			# Load picture file (blurred or original)
			if fs.isfile(blurPicPath):
				picture = fs.openbin(blurPicPath)
				picFormat = "webp"
			else:
				picture = fs.openbin(picPath)
				picFormat = "jpeg"
		except:
			raise errors.InvalidAPIUsage("Unable to read picture on filesystem", status_code=500)

		return sendInFormat(picture, picFormat, format)


def createSDPicture(fs, picture, outputFilename, webpMethod=6):
	"""Create a standard definition version of given picture and save it on filesystem

	Parameters
	----------
	fs : fs.base.FS
		Filesystem to look through
	picture : PIL.Image
		Input image
	outputFilename : str
		Path to output file (relative to instance root)

	Returns
	-------
	bool
		True if operation was successful
	"""

	sdImg = picture.resize((2048, int(picture.size[1]*2048/picture.size[0])))

	sdImgBytes = io.BytesIO()
	sdImg.save(sdImgBytes, format="webp", method=webpMethod, quality=70)
	fs.writebytes(outputFilename, sdImgBytes.getvalue())

	return True


@bp.route('/<uuid:pictureId>/sd.<format>')
def getPictureSD(pictureId, format):
	"""Get picture image (standard definition)
	---
		parameters:
			- name: pictureId
			  in: path
			  type: string
			  description: ID of picture to retrieve
			- name: format
			  in: path
			  type: string
			  description: Wanted format for output image (either jpg or webp)
		responses:
			200:
				description: Standard definition (width of 2048px)
				content:
					image/jpeg:
						schema:
							type: string
							format: binary
					image/webp:
						schema:
							type: string
							format: binary
	"""

	checkFormatParam(format)

	with open_fs(current_app.config['FS_URL']) as fs:
		checkPictureStatus(fs, pictureId)
		picPath = getDerivatesPath(pictureId) + "/sd.webp"
		picture = None
		try:
			picture = fs.openbin(picPath)
		except:
			raise errors.InvalidAPIUsage("Unable to read picture on filesystem", status_code=500)

		return sendInFormat(picture, "webp", format)


def createThumbPicture(fs, picture, outputFilename, type = "equirectangular", webpMethod=6):
	"""Create a thumbnail version of given picture and save it on filesystem

	Parameters
	----------
	fs : fs.base.FS
		Filesystem to look through
	picture : PIL.Image
		Input image
	outputFilename : str
		Path to output file (relative to instance root)
	type : str (optional)
		Type of picture (flat, equirectangular (default))

	Returns
	-------
	bool
		True if operation was successful
	"""

	if type == "equirectangular":
		tbImg = picture.resize((2000, 1000)).crop((750, 350, 1250, 650))
	else:
		tbImg = picture.resize((500, int(picture.size[1]*500/picture.size[0])))

	tbImgBytes = io.BytesIO()
	tbImg.save(tbImgBytes, format="webp", method=webpMethod, quality=70)
	fs.writebytes(outputFilename, tbImgBytes.getvalue())

	return True


@bp.route('/<uuid:pictureId>/thumb.<format>')
def getPictureThumb(pictureId, format):
	"""Get picture thumbnail
	---
		parameters:
			- name: pictureId
			  in: path
			  type: string
			  description: ID of picture to retrieve
			- name: format
			  in: path
			  type: string
			  description: Wanted format for output image (either jpg or webp)
		responses:
			200:
				description: 500px wide ready-for-display image
				content:
					image/jpeg:
						schema:
							type: string
							format: binary
					image/webp:
						schema:
							type: string
							format: binary
	"""

	checkFormatParam(format)

	with open_fs(current_app.config['FS_URL']) as fs:
		checkPictureStatus(fs, pictureId)
		picPath = getDerivatesPath(pictureId) + "/thumb.webp"
		picture = None
		try:
			picture = fs.openbin(picPath)
		except:
			raise errors.InvalidAPIUsage("Unable to read picture on filesystem", status_code=500)

		return sendInFormat(picture, "webp", format)


def getTileSize(imgSize):
	"""Compute ideal amount of rows and columns to give a tiled version of an image according to its original size

	Parameters
	----------
	imgSize : tuple
		Original image size, as (width, height)

	Returns
	-------
	tuple
		Ideal tile splitting as (cols, rows)
	"""

	possibleCols = [4,8,16,32,64] # Limitation of PSV, see https://photo-sphere-viewer.js.org/guide/adapters/tiles.html#cols-required
	idealCols = max(min(int(int(imgSize[0] / 512) / 2) * 2, 64), 4)
	cols = possibleCols[0]
	for c in possibleCols:
		if idealCols >= c:
			cols = c
	return (int(cols), int(cols/2))


def createTiledPicture(fs, picture, destPath, cols, rows, webpMethod=6):
	"""Create tiled version of an input image into destination directory.

	Output images are named following col_row.webp format, 0_0.webp being the top-left corner.

	Parameters
	----------
	fs : fs.base.FS
		Filesystem to look through
	picture : PIL.Image
		Input image
	destPath : str
		Path of the output directory
	cols : int
		Amount of columns for splitted image
	rows : int
		Amount of rows for splitted image
	"""

	colWidth = math.floor(picture.size[0] / cols)
	rowHeight = math.floor(picture.size[1] / rows)

	def createTile(picture, col, row):
		tilePath = destPath + "/" + str(col) + "_" + str(row) + ".webp"
		tile = picture.crop((colWidth * col, rowHeight * row, colWidth * (col+1), rowHeight * (row+1)))
		tileBytes = io.BytesIO()
		tile.save(tileBytes, format="webp", method=webpMethod, quality=80)
		fs.writebytes(tilePath, tileBytes.getvalue())
		return True

	with ThreadPoolExecutor(max_workers=os.cpu_count()) as executor:
		futures = []
		for col in range(cols):
			for row in range(rows):
				futures.append(executor.submit(createTile, picture.copy(), col, row))
			for future in as_completed(futures):
				pass

	return True


@bp.route('/<uuid:pictureId>/tiled/<int:col>_<int:row>.<format>')
def getPictureTile(pictureId, col, row, format):
	"""Get picture tile
	---
		parameters:
			- name: pictureId
			  in: path
			  type: string
			  description: ID of picture to retrieve
			- name: col
			  in: path
			  type: number
			  description: Tile column ID
			- name: row
			  in: path
			  type: number
			  description: Tile row ID
			- name: format
			  in: path
			  type: string
			  description: Wanted format for output image (either jpg or webp)
		responses:
			200:
				description: Tile image (size depends of original image resolution, square with side size around 512px)
				content:
					image/jpeg:
						schema:
							type: string
							format: binary
					image/webp:
						schema:
							type: string
							format: binary
	"""

	checkFormatParam(format)

	with open_fs(current_app.config['FS_URL']) as fs:
		metadata = checkPictureStatus(fs, pictureId)
		picPath = f"{getDerivatesPath(pictureId)}/tiles/{col}_{row}.webp"

		if metadata['type'] == "flat":
			raise errors.InvalidAPIUsage("Tiles are not available for flat pictures", status_code=404)
		if col < 0 or col >= metadata['cols']:
			raise errors.InvalidAPIUsage("Column parameter is invalid", status_code=404)
		if row < 0 or row >= metadata['rows']:
			raise errors.InvalidAPIUsage("Column parameter is invalid", status_code=404)

		picture = None
		try:
			picture = fs.openbin(picPath)
		except:
			raise errors.InvalidAPIUsage("Unable to read picture on filesystem", status_code=500)

		return sendInFormat(picture, "webp", format)
