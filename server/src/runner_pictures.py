import re
import datetime
from fs import open_fs
from fs.errors import ResourceNotFound
from PIL import Image, ExifTags
from flask import current_app
import psycopg
import traceback
import os
import gc
import io
import random
import xmltodict
from psycopg.types.json import Jsonb
from collections import Counter
from src import pictures
from concurrent.futures import as_completed, ThreadPoolExecutor
from itertools import repeat
from tqdm import tqdm

def run():
	"""Launch processing of pictures and sequences"""

	with open_fs(current_app.config['FS_URL']) as fs:
		with psycopg.connect(current_app.config['DB_URL'], autocommit=True) as db:
			seqFolders = listSequencesToProcess(fs, db)
			nbSeq = len(seqFolders)

			if nbSeq > 0:
				print(nbSeq, "sequences to process")
				currentSeq = 1

				for seqFolder in seqFolders:
					processSequence(fs, db, seqFolder, f"({currentSeq}/{nbSeq})")
					currentSeq += 1

				print("Done processing", len(seqFolders), "sequences")
			else:
				print("No sequence to process for now")

			dbPostProcessOptimize(db)


def sequencesNamesToIds(fs, db, sequences):
	"""Transforms a list of sequences names into their IDs"""

	invalid = False
	valid = []
	for seq in sequences:
		if ('/' in seq) or (not fs.isdir("/" + seq)) or (skipSequenceFolder(seq)):
			print("Sequence name '%s' not found, please use a valid sequence folder name" % seq)
			invalid = True
		else:
			valid.append(seq)

	if invalid:
		raise Exception("Some sequences names are invalid, please check names and try again")
	else:
		return [ r[0] for r in db.execute("SELECT id FROM sequences WHERE folder_path = ANY(%s)", [valid]).fetchall() ]


def setSequencesHeadings(sequences, value, overwrite):
	with open_fs(current_app.config['FS_URL']) as fs:
		with psycopg.connect(current_app.config['DB_URL'], autocommit=True) as db:
			sequences = sequencesNamesToIds(fs, db, sequences)

			if len(sequences) == 0:
				print("Updating all sequences")
				sequences = [ r[0] for r in db.execute("SELECT id FROM sequences").fetchall() ]

			for seq in sequences:
				updateSequenceHeadings(db, seq, value, not overwrite)

			print("Done processing %s sequences" % len(sequences))


def redoSequences(sequences):
	"""Reprocesses the files associated with the given sequences.

	This is a CLI entry point.

	Sequences entries are not modified in the database (except for there status),
	meaning uids are kept. The same is true for images, unless they are new or
	no longer exist. Sequences cannot be deleted this way.

	Because uids must remain the same, a simple cleanup()+process() cannot be used.

	*Beware*, this method does not update pictures metadata, meaning that if a
	sequence's picture files are modified, there must not be a new picture with
	the same name as an old one, otherwise the new one will have the same metadata,
	time stamp, position and heading in the database.

	Parameters
	----------
	sequences : [str]
		the sequences names to process again
	"""
	if len(sequences) == 0:
		print("No sequence name given")
		return

	print("Reprocessing", len(sequences), "sequences...")

	with open_fs(current_app.config['FS_URL']) as fs:
		with psycopg.connect(current_app.config['DB_URL'], autocommit=True) as db:
			for seq in sequences:
				if ('/' in seq) or (not fs.isdir("/" + seq)) or (skipSequenceFolder(seq)):
					print("Sequence name '%s' not found, please use a valid sequence folder name" % seq)
					continue

				storedSequence = db.execute("SELECT id FROM sequences where folder_path = %s", [ seq ]).fetchone()

				if storedSequence is None:
					processSequence(fs, db, seq)
				else:
					reprocessSequence(fs, db, seq, storedSequence[0])

			print("Done reprocessing %d sequences" % len(sequences))


def removeFsEvenNotFound(fs, path):
	"""Deletes file from given fs without raising ResourceNotFound exception"""
	try:
		fs.remove(path)
	except ResourceNotFound:
		pass

def removeFsTreeEvenNotFound(fs, path):
	"""Deletes tree from given fs without raising ResourceNotFound exception"""
	try:
		fs.removetree(path)
	except ResourceNotFound:
		pass


def cleanup(sequences=[], full=False, database=False, cache=False, blur_masks=False):
	"""Cleans up various data or files of GeoVisio

	Parameters
	----------
	sequences : list
		List of sequences names to clean-up. If none is given, all sequences are cleaned up.
	full : bool
		For full cleaning (deletes DB entries and derivates files including blur masks)
	database : bool
		For removing database entries without deleting derivates files
	cache : bool
		For removing derivates files (except blur masks)
	blur_masks : bool
		For removing only blur masks
	"""

	if full:
		database = True
		cache = True
		blur_masks = True

	if database is False and cache is False and blur_masks is False:
		return True

	allSequences = len(sequences) == 0

	with psycopg.connect(current_app.config['DB_URL'], autocommit=True) as conn:
		with open_fs(current_app.config['FS_URL']) as fs:
			if allSequences:
				pictures = [ str(p[0]) for p in conn.execute("SELECT id FROM pictures").fetchall() ]
			else:
				# Find sequences ID based on their folder path
				sequences = sequencesNamesToIds(fs, conn, sequences)

				# Find pictures in sequences to cleanup
				pictures = [ str(p[0]) for p in conn.execute("""
					WITH pic2rm AS (
						SELECT DISTINCT pic_id FROM sequences_pictures WHERE seq_id = ANY(%(seq)s)
					)
					SELECT * FROM pic2rm
					EXCEPT
					SELECT DISTINCT pic_id FROM sequences_pictures WHERE pic_id IN (SELECT * FROM pic2rm) AND seq_id != ANY(%(seq)s)
				""", { "seq": sequences }).fetchall() ]

			if database:
				print("Cleaning up database...")
				if allSequences:
					conn.execute("DELETE FROM sequences_pictures")
					conn.execute("DELETE FROM sequences")
					conn.execute("DELETE FROM pictures")
				else:
					conn.execute("DELETE FROM sequences_pictures WHERE seq_id = ANY(%s)", [sequences])
					conn.execute("DELETE FROM sequences WHERE id = ANY(%s)", [sequences])
					conn.execute("DELETE FROM pictures WHERE id = ANY(%s)", [pictures])

				conn.close()

			if allSequences and cache and blur_masks:
				print("Cleaning up derivates files...")
				removeFsTreeEvenNotFound(fs, current_app.config['PIC_DERIVATES_DIR'])
			else:
				if cache:
					print("Cleaning up derivates cache files...")
					for picId in pictures:
						picPath = f"{current_app.config['PIC_DERIVATES_DIR']}/{picId[0:2]}/{picId}"
						removeFsEvenNotFound(fs, picPath+"/blurred.webp")
						removeFsEvenNotFound(fs, picPath+"/thumb.webp")
						removeFsEvenNotFound(fs, picPath+"/sd.webp")
						removeFsTreeEvenNotFound(fs, picPath+"/tiles")

						# Even if not used since version >= 1.4.0, we clean JPEG old files just in case
						removeFsEvenNotFound(fs, picPath+"/blurred.jpg")
						removeFsEvenNotFound(fs, picPath+"/thumb.jpg")
						removeFsEvenNotFound(fs, picPath+"/sd.jpg")
						removeFsEvenNotFound(fs, picPath+"/progressive.jpg")

				if blur_masks:
					print("Cleaning up blur masks...")
					for picId in pictures:
						removeFsEvenNotFound(fs, f"{current_app.config['PIC_DERIVATES_DIR']}/{picId[0:2]}/{picId}/blur_mask.png")

				# Remove empty pictures folders
				for picId in pictures:
					picPath = f"{current_app.config['PIC_DERIVATES_DIR']}/{picId[0:2]}/{picId}"
					if fs.isdir(picPath) and fs.isempty(picPath):
						fs.removedir(picPath)

				# Remove empty group of pictures folders
				for picGroup in fs.listdir(current_app.config['PIC_DERIVATES_DIR']):
					picGroupPath = f"{current_app.config['PIC_DERIVATES_DIR']}/{picGroup}"
					if fs.isempty(picGroupPath):
						fs.removedir(picGroupPath)

	print("Cleanup done")
	return True


def skipSequenceFolder(folder):
	"""Based on its name, should a sequence folder should be skipped

	Parameters
	----------
	folder : str
		Folder name

	Returns
	-------
	bool
		True if folder may be skipped from processing
	"""

	return folder.startswith("gvs_") or folder.startswith("geovisio_") or folder.startswith("ignore_")


def listSequencesToProcess(fs, db):
	"""Find new sequence folders to process

	Parameters
	----------
	fs : fs.base.FS
		Filesystem to look through
	db : psycopg.Connection
		Database connection

	Returns
	-------
	list
		List of sequence folders to import
	"""

	sequencesFolders = [ x for x in fs.listdir("/") if fs.isdir("/" + x) and not skipSequenceFolder(x) ]
	sequencesInDb = db.execute("SELECT array_agg(folder_path) FROM sequences").fetchone()[0]
	diff = Counter(sequencesFolders) - Counter(sequencesInDb)
	return list(diff.elements())


def getProgressBar(picLength):
	"""Creates a clean progress bar for pictures processing"""

	return tqdm(total = picLength, unit = "pics", smoothing = 0.7)


def processSequence(fs, db, sequenceFolder, globalProgress = None):
	"""Processes a single sequence (list pictures, read metadata, populate database)

	Parameters
	----------
	fs : fs.base.FS
		Filesystem to look through
	db : psycopg.Connection
		Database connection
	sequenceFolder : str
		Name of the sequence folder (relative to instance root)
	globalProgress : str
		String to display global progress, or None to not show
	"""

	# List available pictures in sequence folder
	picturesFilenames = sorted([
		f for f in fs.listdir(sequenceFolder)
		if re.search(r'\.jpe?g$', f, re.IGNORECASE)
	])

	if len(picturesFilenames) > 0:
		# Create sequence in database
		seqId = db.execute("INSERT INTO sequences(folder_path) VALUES(%s) RETURNING id", [sequenceFolder]).fetchone()[0]

		# Process pictures
		maxWorkers = getMaxWorkerThreads(current_app.config)
		print(f"Processing {len(picturesFilenames)} pictures in sequence {sequenceFolder}{' '+globalProgress if globalProgress is not None else ''} using {maxWorkers} workers")
		picProcessFailures = 0
		picIds = []

		if current_app.config['BLUR_STRATEGY'] != "DISABLE":
			from src.blur.blur import blurPreinit
			blurPreinit(current_app.config)

		with getProgressBar(len(picturesFilenames)) as progress:
			with ThreadPoolExecutor(max_workers=maxWorkers) as executor:
				picProcResults = executor.map(processNewPicture, repeat(fs), repeat(db), repeat(sequenceFolder), picturesFilenames, repeat(current_app.config), repeat(progress))

				for picProcRes in picProcResults:
					if picProcRes['status'] == "broken":
						picProcessFailures += 1
					else:
						picIds.append(picProcRes['id'])

		if picProcessFailures > 0:
			print(picProcessFailures, "pictures failed in sequence", sequenceFolder)

		if picProcessFailures == len(picturesFilenames):
			# Remove sequence if all its pictures failed
			db.execute("DELETE FROM sequences WHERE id = %s", [seqId])
			print("Import of sequence", sequenceFolder,"is cancelled (all pictures failed)")
		else:
			# Update sequence metadata and status
			# Create link between pictures and sequence
			with db.cursor() as cursor:
				with cursor.copy("COPY sequences_pictures(seq_id, rank, pic_id) FROM STDIN") as copy:
					for i, p in enumerate(picIds):
						copy.write_row((seqId, i+1, p))

			# Process field of view for each pictures
			picMake, picModel, picType = db.execute("""
				SELECT metadata->>'make' AS make, metadata->>'model' AS model, metadata->>'type' AS type
				FROM pictures p
				JOIN sequences_pictures sp ON sp.seq_id = %s AND p.id = sp.pic_id AND rank = 1
			""", [seqId]).fetchone()

			# Flat pictures = variable fov
			if picType == 'flat':
				if picMake is not None and picModel is not None:
					db.execute("SET pg_trgm.similarity_threshold = 0.9");
					db.execute("""
						UPDATE pictures
						SET metadata = jsonb_set(metadata, '{field_of_view}'::text[], COALESCE(
							(
								SELECT ROUND(DEGREES(2 * ATAN(sensor_width / (2 * (metadata->>'focal_length')::float))))::varchar
								FROM cameras
								WHERE model %% CONCAT(%(make)s::text, ' ', %(model)s::text)
								ORDER BY model <-> CONCAT(%(make)s::text, ' ', %(model)s::text)
								LIMIT 1
							),
							'null'
						)::jsonb)
						WHERE id = ANY (SELECT pic_id FROM sequences_pictures WHERE seq_id = %(seq)s)
					""", { "seq": seqId, "make": picMake, "model": picModel });

			# 360 pictures = 360° fov
			else:
				db.execute("""
					UPDATE pictures
					SET metadata = jsonb_set(metadata, '{field_of_view}'::text[], '360'::jsonb)
					WHERE id = ANY (SELECT pic_id FROM sequences_pictures WHERE seq_id = %s)
				""", [seqId])

			# Complete missing headings in pictures
			updateSequenceHeadings(db, seqId)

			# Change sequence database status in DB
			db.execute("""
				UPDATE sequences
				SET status = 'ready', geom = ST_MakeLine(ARRAY(
					SELECT p.geom
					FROM sequences_pictures sp
					JOIN pictures p ON sp.pic_id = p.id
					WHERE sp.seq_id = %(seq)s
					ORDER BY sp.rank
				))
				WHERE id = %(seq)s
			""", { "seq": seqId })
			print("Sequence", sequenceFolder, "is ready")
	else:
		print("Skipped empty sequence", sequenceFolder)


def reprocessSequence(fs, db, sequenceFolder, sequenceId):
	"""Re-processes a sequence that already exists in the database.

	Derivate pictures are re-generated and the database is updated.

	IDs of existing picture entries are unmodified unless there images have been
	removed from the sequence, in which case `pictures` and `sequences_pictures`
	entries are removed.

	Parameters
	----------
	fs : fs.base.FS
		Filesystem to look through
	db : psycopg.Connection
		Database connection
	sequenceFolder : str
		Name of the sequence folder (relative to instance root)
	pictureFilename : str
		Name of the picture file (relative to sequence folder)
	sequenceId : uuid
		The sequence's uuid, as stored in the database
	"""

	db.execute("UPDATE sequences SET status = 'preparing' WHERE id = %s", [ sequenceId ])

	# collect all pictures to process for sequence

	newPictures = [
		f for f in fs.listdir(sequenceFolder)
		if re.search(r'\.jpe?g$', f, re.IGNORECASE)
	]

	existingPictures = db.execute("""SELECT file_path
		FROM sequences_pictures INNER JOIN pictures ON (pictures.id = sequences_pictures.pic_id)
		WHERE seq_id = %s""", [ sequenceId ]).fetchall()

	allPictures = []
	allPictures.extend(newPictures)

	for (path,) in existingPictures:
		picName = path.split('/')[-1]
		if picName not in allPictures:
			allPictures.append(picName)

	# Process pictures
	if current_app.config['BLUR_STRATEGY'] != "DISABLE":
		from src.blur.blur import blurPreinit
		blurPreinit(current_app.config)
	maxWorkers = getMaxWorkerThreads(current_app.config)
	print("Reprocessing", len(newPictures), "pictures (was "+str(len(existingPictures))+") in sequence", sequenceFolder, "using", maxWorkers, "workers")

	db.execute("DELETE FROM sequences_pictures WHERE seq_id = %s", [ sequenceId ])

	databasePictures = []
	failCount = 0

	with getProgressBar(len(allPictures)) as progress:
		with ThreadPoolExecutor(max_workers=maxWorkers) as executor:
			picProcResults = executor.map(reprocessPicture, repeat(fs), repeat(db), repeat(sequenceFolder), allPictures, repeat(sequenceId), repeat(current_app.config), repeat(progress))

			for picName, result in zip(allPictures, picProcResults):
				if 'picId' in result:
					databasePictures.append((picName, result['picId']))
				if result['status'] == 'fail':
					failCount += 1

	databasePictures.sort() # sort pictures by name

	# if all pictures failed, set the sequence status to 'broken' and stop
	if failCount > 0:
		print(failCount, "pictures failed in sequence", sequenceFolder)
	if len(databasePictures) == 0:
		print("Reprocess of sequence", sequenceFolder, "has failed (no valid picture remains)")
		db.execute("UPDATE sequences SET status = 'broken' WHERE id = %s", [ sequenceId ])
		return

	# update ranks
	with db.cursor() as cursor:
		with cursor.copy("COPY sequences_pictures(seq_id, rank, pic_id) FROM STDIN") as copy:
			for i,(pictureName, pictureId) in enumerate(databasePictures):
				copy.write_row((sequenceId, i+1, pictureId))

	# Complete missing headings in pictures
	updateSequenceHeadings(db, sequenceId)

	# update the sequence status
	db.execute("UPDATE sequences SET status = 'ready' WHERE id = %s", [ sequenceId ])


def dbPostProcessOptimize(db):
	"""Runs various cleanup request against database"""
	print("Optimizing database...")
	db.execute("VACUUM (ANALYZE, INDEX_CLEANUP OFF)")
	db.execute("REINDEX TABLE pictures")
	db.execute("REINDEX TABLE sequences")
	db.execute("REINDEX TABLE sequences_pictures")


def processNewPicture(fs, db, sequenceFolder, pictureFilename, config, progress = None):
	"""Processes a single picture (read file, extract metadata, generate derivate files, insert in database)

	Parameters
	----------
	fs : fs.base.FS
		Filesystem to look through
	db : psycopg.Connection
		Database connection
	sequenceFolder : str
		Name of the sequence folder (relative to instance root)
	pictureFilename : str
		Name of the picture file (relative to sequence folder)
	config : dict
		Flask app.config (passed as param to allow using ThreadPoolExecutor)
	progress : tqdm
		Progress bar handler (optional)

	Returns
	-------
	{ id : uuid, status : "ready"|"broken" }
		if a new picture entry was successfully added in the database
	False
		if the picture could not be added to the database (because it has
		no exif info for example)
	"""

	picId = None
	picProcessStatus = None

	try:
		picture = Image.open(fs.openbin(sequenceFolder + "/" + pictureFilename))

		# create the picture entry in the database
		picId = insertNewPictureInDatabase(db, sequenceFolder, pictureFilename, picture)
		picProcessStatus = "preparing"

		# Generate derivated picture versions
		processPictureFiles(fs, picId, picture, config)
		picProcessStatus = "ready"

	except Exception as e:
		picProcessStatus = "broken"
		printPictureProcessingError(e, sequenceFolder, pictureFilename)

	# Mark picture as ready in database
	finally:
		if picId is not None:
			db.execute("UPDATE pictures SET status = %s WHERE id = %s", (picProcessStatus, picId))
		if progress is not None:
			progress.update()
		return { "id": picId, "status": picProcessStatus }


def reprocessPicture(fs, db, sequenceFolder, pictureFilename, sequenceId, config, progress = None):
	"""Reprocesses a picture.

	The picture file may or may not exist in the database and may or may not exist in the
	file system. It will be created/deleted/updated in the database and if it exists on disk
	its derivate files will be regenerated.

	Parameters
	----------
	fs : fs.base.FS
		Filesystem to look through
	db : psycopg.Connection
		Database connection
	sequenceFolder : str
		Name of the sequence folder (relative to instance root)
	pictureFilename : str
		Name of the picture file (relative to sequence folder)
	sequenceId : uuid
		The sequence's uuid, as stored in the database
	config : dict
		Flask app.config (passed as param to allow using ThreadPoolExecutor)
	progress : tqdm
		Progress bar handler (optional)

	Returns
	-------
	{
		status: "fail" | "deleted" | "success",
		picId?: uuid
	}
	picIs is only specified if there is a picture entry in the database (it wasn't deleted/it was successfully created)
	"""
	picturePath = sequenceFolder + '/' + pictureFilename
	pictureExistsNow = fs.exists(picturePath)
	pictureAlreadyExists = False
	pictureId = None
	pictureObject = None

	existingEntry = db.execute('SELECT id FROM pictures WHERE file_path = %s', [ picturePath ]).fetchall()
	if len(existingEntry) != 0:
		pictureId = existingEntry[0][0]
		pictureAlreadyExists = True

	if pictureExistsNow:
		pictureObject = Image.open(fs.openbin(picturePath))

	if pictureExistsNow and pictureAlreadyExists:
		# only reprocess files
		db.execute("UPDATE pictures SET status = 'preparing' WHERE id = %s", [ pictureId ])

		if progress is None:
			print("Updated picture", pictureFilename, "in sequence", sequenceFolder)
	elif pictureExistsNow:
		# insert in database
		try:
			pictureId = insertNewPictureInDatabase(db, sequenceFolder, pictureFilename, pictureObject)

			if progress is None:
				print("New picture", pictureFilename, "in sequence", sequenceFolder)
		except Exception as e:
			printPictureProcessingError(e, sequenceFolder, pictureFilename)
			return { 'status': 'fail' }
	else:
		# remove from database and remove generate files
		db.execute("DELETE FROM pictures WHERE file_path = %s", [ picturePath ])
		picDerivatesParent = f"{config['PIC_DERIVATES_DIR']}/{str(pictureId)[0:2]}"
		picDerivatesFolder = f"{picDerivatesParent}/{pictureId}"
		if fs.exists(picDerivatesFolder):
			fs.removetree(picDerivatesFolder)
			if fs.isempty(picDerivatesParent):
				fs.removedir(picDerivatesParent)

		if progress is None:
			print("Removed picture", pictureFilename, "in sequence", sequenceFolder)

		return { 'status': 'deleted' }

	try:
		processPictureFiles(fs, pictureId, pictureObject, config)
		db.execute("UPDATE pictures SET status = 'ready' WHERE id = %s", [ pictureId ])

		if progress is not None:
			progress.update()

		return { 'status': 'success', 'picId': pictureId }
	except Exception as e:
		printPictureProcessingError(e, sequenceFolder, pictureFilename)
		db.execute("UPDATE pictures SET status = 'broken' WHERE id = %s", [ pictureId ])

		if progress is not None:
			progress.update()

		return { 'status': 'fail', 'picId': pictureId }


def insertNewPictureInDatabase(db, sequenceFolder, pictureFilename, picture):
	"""Inserts a new 'pictures' entry in the database, from a picture file.
	Database is not committed in this function, to make entry definitively stored
	you have to call db.commit() after or use an autocommit connection.
	Also, picture is by default in state "preparing", so you may want to update
	this as well after function run.

	Parameters
	----------
	db : psycopg.Connection
		Database connection
	sequenceFolder : str
		Name of the sequence folder (relative to instance root)
	pictureFilename : str
		Name of the picture file (relative to sequence folder)
	picture : PIL.Image
		Image file in Pillow format

	Returns
	-------
	uuid : The uuid of the new picture entry in the database
	"""

	# Create a fully-featured metadata object
	metadata = readPictureMetadata(picture) | pictures.getPictureSizing(picture)

	# Remove cols/rows information for flat pictures
	if metadata["type"] == "flat":
		metadata.pop("cols")
		metadata.pop("rows")

	# Add picture metadata to database (with "preparing" status)
	picId = db.execute("""
		INSERT INTO pictures (file_path, ts, heading, metadata, geom)
		VALUES (%s, to_timestamp(%s), %s, %s, ST_SetSRID(ST_MakePoint(%s, %s), 4326))
		RETURNING id
	""", (
		sequenceFolder + "/" + pictureFilename,
		metadata['ts'],
		metadata['heading'],
		Jsonb(metadata),
		metadata['lon'],
		metadata['lat']
	)).fetchone()[0]

	return picId


def updateSequenceHeadings(db, sequenceId, relativeHeading = 0, updateOnlyMissing = True):
	"""Defines pictures heading according to sequence path.
	Database is not committed in this function, to make entry definitively stored
	you have to call db.commit() after or use an autocommit connection.

	Parameters
	----------
	db : psycopg.Connection
		Database connection
	sequenceId : uuid
		The sequence's uuid, as stored in the database
	relativeHeading : int
		Camera relative orientation compared to path, in degrees clockwise.
		Example: 0° = looking forward, 90° = looking to right, 180° = looking backward, -90° = looking left.
	updateOnlyMissing : bool
		If true, doesn't change existing heading values in database
	"""

	db.execute("""
		WITH h AS (
			SELECT
				p.id,
				CASE
					WHEN LEAD(sp.rank) OVER othpics IS NULL AND LAG(sp.rank) OVER othpics IS NULL
						THEN NULL
					WHEN LEAD(sp.rank) OVER othpics IS NULL
						THEN (360 + FLOOR(DEGREES(ST_Azimuth(LAG(p.geom) OVER othpics, p.geom)))::int + (%(diff)s %% 360)) %% 360
					ELSE
						(360 + FLOOR(DEGREES(ST_Azimuth(p.geom, LEAD(p.geom) OVER othpics)))::int + (%(diff)s %% 360)) %% 360
				END AS heading
			FROM pictures p
			JOIN sequences_pictures sp ON sp.pic_id = p.id AND sp.seq_id = %(seq)s
			WINDOW othpics AS (ORDER BY sp.rank)
		)
		UPDATE pictures p
		SET
			heading = h.heading,
			metadata = jsonb_set(metadata, '{heading}'::text[], (h.heading::text)::jsonb)
		FROM h
		WHERE h.id = p.id
		""" + (" AND p.heading IS NULL " if updateOnlyMissing else ""),
		{ "seq": sequenceId, "diff": relativeHeading }
	)


def processPictureFiles(fs, picId, picture, config):
	"""Generates the files associated with a sequence picture.

	If needed the image is blurred before the tiles and thumbnail are generated.

	Parameters
	----------
	fs : fs.base.FS
		Filesystem to look through
	picId : uuid
		The picture's uuid, as stored in the database
	picture : PIL.Image
		The picture to process
	config : dict
		Flask app.config (passed as param to allow using ThreadPoolExecutor)
	"""
	metadata = readPictureMetadata(picture) | pictures.getPictureSizing(picture)

	# Create picture derivates folder for this specific picture
	picDerivatesFolder = f"{config['PIC_DERIVATES_DIR']}/{str(picId)[0:2]}/{picId}"
	if config['DERIVATES_STRATEGY'] == "PREPROCESS" or config['BLUR_STRATEGY'] != "DISABLE":
		fs.makedirs(picDerivatesFolder, recreate=True)

	# Create blurred version if required
	if config['BLUR_STRATEGY'] != "DISABLE":
		from src.blur.blur import getBlurMask
		blurMask = getBlurMask(picture)

		# Write blur mask
		blurMaskBytes = io.BytesIO()
		blurMask.save(blurMaskBytes, format="png", optimize=True, bits=1)
		fs.writebytes(picDerivatesFolder + "/blur_mask.png", blurMaskBytes.getvalue())

		# For some reason garbage collection does not run automatically after
		# a call to an AI model, so it must be done explicitely
		gc.collect()

		# Write blurred picture
		if config['DERIVATES_STRATEGY'] == "PREPROCESS":
			picture = pictures.createBlurredHDPicture(fs, picture, blurMask, picDerivatesFolder + "/blurred.webp")

	# Create SD, thumbnail and tiles
	if config['DERIVATES_STRATEGY'] == "PREPROCESS":
		pictures.generatePictureDerivates(fs, picture, metadata, picDerivatesFolder, metadata["type"])


def readPictureMetadata(picture):
	"""Extracts metadata from picture file

	Parameters
	----------
	picture : PIL.Image
		Picture file

	Returns
	-------
	dict
		Various metadata fields : lat, lon, ts, heading, type, make, model, focal_length
	"""

	data = {}

	info = picture._getexif()
	if info:
		for tag, value in info.items():
			decoded = ExifTags.TAGS.get(tag, tag)
			if decoded == "GPSInfo":
				for t in value:
					sub_decoded = ExifTags.GPSTAGS.get(t, t)
					data[sub_decoded] = value[t]
			else:
				data[decoded] = value

	# Read XMP tags
	for segment, content in picture.applist:
		marker, body = content.split(b'\x00', 1)
		if segment == 'APP1' and marker == b'http://ns.adobe.com/xap/1.0/':
			data = data | xmltodict.parse(body)["x:xmpmeta"]["rdf:RDF"]["rdf:Description"]

	# Cleanup XMP tags with @
	for k in list(data):
		if k.startswith("@"):
			data[k[1:]] = data[k]
			del data[k]

	# Parse latitude/longitude
	if 'GPSLatitude' in data:
		latRaw = data['GPSLatitude']
		lat = (-1 if data['GPSLatitudeRef'] == 'S' else 1) * (float(latRaw[0]) + float(latRaw[1]) / 60 + float(latRaw[2]) / 3600)

		lonRaw = data['GPSLongitude']
		lon = (-1 if data['GPSLongitudeRef'] == 'W' else 1) * (float(lonRaw[0]) + float(lonRaw[1]) / 60 + float(lonRaw[2]) / 3600)
	else:
		raise Exception("No GPS coordinates in picture EXIF tags")

	# Parse date/time
	if 'GPSTimeStamp' in data:
		timeRaw = data['GPSTimeStamp']
		dateRaw = data['GPSDateStamp'].replace(":", "-")
		msRaw = data['SubSecTimeOriginal'] if 'SubSecTimeOriginal' in data else "0"
		d = datetime.datetime.combine(
			datetime.date.fromisoformat(dateRaw),
			datetime.time(
				int(timeRaw[0]),
				int(timeRaw[1]),
				int(timeRaw[2]),
				int(msRaw[:6].ljust(6, "0")),
				tzinfo=datetime.timezone.utc
			)
		)
	elif 'DateTimeOriginal' in data:
		dateRaw = data['DateTimeOriginal'].split(" ")[0].replace(":", "-")
		timeRaw = data['DateTimeOriginal'].split(" ")[1].split(":")
		msRaw = data['SubSecTimeOriginal'] if 'SubSecTimeOriginal' in data else "0"
		d = datetime.datetime.combine(
			datetime.date.fromisoformat(dateRaw),
			datetime.time(
				int(timeRaw[0]),
				int(timeRaw[1]),
				int(timeRaw[2]),
				int(msRaw[:6].ljust(6, "0")),
				tzinfo=datetime.timezone.utc
			)
		)
	else:
		raise Exception("No date in picture EXIF tags")

	# Heading
	heading = None
	if "GPano:PoseHeadingDegrees" in data and "GPSImgDirection" in data:
		if float(data["GPSImgDirection"]) > 0 and float(data["GPano:PoseHeadingDegrees"]) == 0:
			heading = int(round(data["GPSImgDirection"]))
		elif float(data["GPSImgDirection"]) == 0 and float(data["GPano:PoseHeadingDegrees"]) > 0:
			heading = int(round(float(data["GPano:PoseHeadingDegrees"])))
		else:
			raise Exception("Contradicting heading values in EXIF PoseHeadingDegrees and GPSImgDirection tags")
	elif "GPano:PoseHeadingDegrees" in data:
		heading = int(round(float(data["GPano:PoseHeadingDegrees"])))
	elif "GPSImgDirection" in data:
		heading = int(round(data["GPSImgDirection"]))

	# Make and model
	make = decodeMakeModel(data['Make']).strip() if 'Make' in data else None
	model = decodeMakeModel(data['Model']).strip() if 'Model' in data else None
	if model is not None and model is not None:
		model = model.replace(make, "").strip()

	return {
		"lat": lat,
		"lon": lon,
		"ts": d.timestamp(),
		"heading": heading,
		"type": data['GPano:ProjectionType'] if "GPano:ProjectionType" in data else "flat",
		"make": make,
		"model": model,
		"focal_length": float(data['FocalLength']) if 'FocalLength' in data else None
	}


def decodeMakeModel(value):
	"""Python 2/3 compatible decoding of make/model field."""
	if hasattr(value, "decode"):
		try:
			return value.decode("utf-8").replace('\x00','')
		except UnicodeDecodeError:
			return None
	else:
		return value.replace('\x00','')


def getMaxWorkerThreads(config):
	"""Dictates how many worker threads can be allocated to the processing of a sequence.

	This number may vary depending on the blur configuration and the BLUR_THREADS_LIMIT environment variable.

	Returns
	-------
	int
		The number of threads to use to process a sequence, always less than the number of available CPUs
	"""
	if config['BLUR_STRATEGY'] == 'DISABLE' or config['BLUR_THREADS_LIMIT'] == 0:
		return os.cpu_count()
	return min(config['BLUR_THREADS_LIMIT'], os.cpu_count())


def printPictureProcessingError(e, sequenceFolder, pictureFilename):
	"""Prints a decorated error on stdout.

	Parameters
	----------
	e : Error
		The error that caused the processing to stop
	sequenceFolder : str
		The sequence folder containing the picture that broke
	pictureFilename : str
		The picture's file name that couldn't be processed
	"""
	print("")
	print("---------------------------------------------------------")
	print("WARNING : an error occured while processing picture '%s/%s'" % (sequenceFolder, pictureFilename))
	print(''.join(traceback.format_exception(type(e), e, e.__traceback__)))
	print("---------------------------------------------------------")
	print("")
