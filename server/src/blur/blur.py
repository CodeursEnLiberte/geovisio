import os, sys
import threading
import rpack

from PIL import Image, ImageFilter

from . import precess


tlocal = threading.local() # thread-local data, used to store models that cannot be shared accross threads
blurStrategy = None


def getThreadLocalModel(modelType):
    """Retrieves the segmentation model local to this thread.

    The model is created if it does not exist.

    Parameters
    ----------
    modelType : Model class
        the constructor that will be used if the model does not exist

    Returns
    -------
    modelType
        the segmenter to use
    """
    global tlocal
    try:
        return tlocal.model
    except AttributeError:
        # load blur model (1 per thread)
        tlocal.model = modelType()
    return tlocal.model


def requireObjectDetectors(config):
    """Ensures that the objectdetectors.py file may be imported safely.

    The file depends on YOLOv6, which is outside of the project's files.
    The path to YOLO files is appended to the PATH.

    Paremeters
    ----------
    config : dict
        The application config, containing the YOLOV6_PATH value
    """
    if 'YOLOV6_PATH' not in config:
        print("Missing environment variable YOLOV6_PATH, YOLO is required for COMPROMISE and QUALITATIVE blur strategies")
        print("Please set the path to the YOLOv6 directory, otherwise use the FAST blur strategy")
        raise Exception("Missing YOLOV6_PATH environment variable")
    sys.path.append(config['YOLOV6_PATH'])


class BlurStrategy(object):
    """A blur strategy is the full process chain that handles blurring an image"""

    def __init__(self, name):
        self.name = name

    def computeBlurMask(self, picture):
        """Computes an image mask of the parts of an image to blur

        Parameters
        ----------
        picture : Image
            The image to blur
        Returns
        -------
            Image : a mask of the regions to blur in the image
        """
        raise RuntimeError('Unimplemented')


class SimpleSegmentationStrategy(BlurStrategy):
    """A strategy that imediately applies a segmentation model to find the areas to blur in a picture"""

    def __init__(self, name, modelProvider, maxReduction=1/40):
        super().__init__(name)
        self.modelProvider = modelProvider
        self.maxReduction = maxReduction

    def computeBlurMask(self, picture):
        model = self.modelProvider()
        return SimpleSegmentationStrategy.computeBlurMaskFromModel(model, picture, self.maxReduction)

    @staticmethod
    def computeBlurMaskFromModel(model, picture, maxReduction=1/40):
        blurMask = Image.new('RGBA', picture.size, (0,0,0,0))
        targetSize = model.getPreferedInputSize()
        margin = .1 # 10% overlap
        downscaledSize = precess.computeBestDownscalling(picture.size, targetSize, margin=margin, maxReduction=maxReduction)
        downscaledPicture = picture.resize(downscaledSize, Image.Resampling.LANCZOS)
        for section in precess.iterQuadSubSections(downscaledSize, targetSize, overlap=int(margin*targetSize[0])):
            segmentationMap = model.runSegmentation(downscaledPicture.crop(box=section))
            blurMaskBox = precess.projectOnDifferentScales(section, downscaledSize, blurMask.size)
            blurMaskSize = (blurMaskBox[2]-blurMaskBox[0], blurMaskBox[3]-blurMaskBox[1])
            scaledSegmap = segmentationMap.resize(blurMaskSize, Image.Resampling.LANCZOS)
            blurMask.paste(scaledSegmap, blurMaskBox, scaledSegmap)
        return blurMask


class DetectionAndSegmentationStrategy(BlurStrategy):
    """A strategy that finds the areas of interest using an object detection
    model and that forwards these regions to a segmentation model while trying
    to reduce to a minimum the work of the segmenter.
    """

    # If an input image is smaller than the segmentation model input size
    # times the no-detection threshold the object detection model won't
    # run and the full image will be forwarded to the segmentation model.
    # This is useful for small images that can be fully fed in one pass
    # of the segmentation model
    NO_DETECTION_THRESHOLD = 2

    # When mergin overlaping boxes, if two boxes do not exactly overlap
    # they can still be merged if the area of the merged box is less than
    # the sum of the areas of the two boxes times the acceptable area loss
    # factor
    ACCEPTABLE_AREA_LOSS_FACTOR = 1.2

    def __init__(self, name, segmentationModelProvider, detectionInfererProvider):
        super().__init__(name)
        self.modelProvider = segmentationModelProvider
        self.infererProvider = detectionInfererProvider

    def computeBlurMask(self, picture):
        model = self.modelProvider()
        inferer = self.infererProvider()

        modelInputSize = model.getPreferedInputSize()

        if picture.size[0] <= self.NO_DETECTION_THRESHOLD * modelInputSize[0] or picture.size[1] <= self.NO_DETECTION_THRESHOLD * modelInputSize[1]:
            # do not run the object detector if the original image is small enough
            # to be fed directly to the segmentation model
            return SimpleSegmentationStrategy.computeBlurMaskFromModel(model, picture)
        else:
            allBoxes = self.collectBoxesInPicture(picture, inferer)
            mergedBoxes = self.mergeBoxes(allBoxes[:], modelInputSize)
            blurredMask = Image.new('RGBA', picture.size, (0,0,0,0))

            if len(mergedBoxes) > 0:
                boxesSegmenterSizes = []
                for x,y,w,h in mergedBoxes:
                    # Theorically, each section found by the inferer can be scalled up/down
                    # before getting processed by the segmenter
                    # For example things in the foregroud could be downscaled because they
                    # don't need that many pixels, and things in the background could be
                    # upscaled such that the segmenter sees theim fully.
                    # It needs to be tested more thoroughly though.

                    boxesSegmenterSizes.append((w,h)) # no scaling
                    # m = max(w,h)
                    # r = 200
                    # boxesSegmenterSizes.append((int(w/m*r), int(h/m*r))) # every box is at most 200px long/high

                mergedBoxesCoords, mergedBoxesQuadSize = self.placeBoxesOnQuad(boxesSegmenterSizes)

                # run the sementic segmenter on the packed boxes
                packed_image = Image.new('RGB', mergedBoxesQuadSize)
                for (x,y,w,h),(sw,sh),(px,py) in zip(mergedBoxes, boxesSegmenterSizes, mergedBoxesCoords):
                    packed_image.paste(picture.crop((x,y,x+w,y+h)).resize((sw,sh)), (px,py))
                blurredPacked = SimpleSegmentationStrategy.computeBlurMaskFromModel(model, packed_image, maxReduction=1/40)

                # unpack the boxes onto a blur mask of the original image
                for (x,y,w,h),(sw,sh),(px,py) in zip(mergedBoxes, boxesSegmenterSizes, mergedBoxesCoords):
                    crop = blurredPacked.crop((px,py,px+sw,py+sh)).resize((w,h))
                    blurredMask.paste(crop, (x,y,x+w,y+h), crop)

            return blurredMask

    def collectBoxesInPicture(self, picture, inferer):
        """Runs an object detection inferer on an image to collect boxes

        Parameters
        ----------
        inferer : Inferer
            The inferer to run on the image
        image : PIL.Image
            The image to detect things in

        Returns
        -------
        [(x,y,w,h)] : The set of boxes detected by the inferer
        """
        S = 2
        targetSize = (640*S, 640*S)
        allBoxes = []
        downscaledSize = precess.computeBestDownscalling(picture.size, targetSize)
        downscaledPicture = picture.resize(downscaledSize, Image.Resampling.LANCZOS)
        for section in precess.iterQuadSubSections(downscaledSize, targetSize, overlap=int(targetSize[0]*.1)):
            sectionCrop = downscaledPicture.crop(section)
            detectedBoxes = inferer.infer(sectionCrop)
            for (_detectedType, bx,by,bw,bh, _condidenceLevel) in detectedBoxes:
                inSectionBoxCoords = self.fromInfererCoordsToImageCoords((bx,by,bw,bh), targetSize) # 0,0..targetSize
                box = list(inSectionBoxCoords)                                                      # 0,0..targetSize
                box[0] += section[0]; box[1] += section[1]                                          # section.x1y1..section.x2y2
                box = precess.projectOnDifferentScales(box, downscaledSize, picture.size)           # 0,0..picture.size
                allBoxes.append(box)
        return allBoxes


    def fromInfererCoordsToImageCoords(self, infererCoords, imageSize, margin=.15):
        x,y,w,h = infererCoords
        px = x*imageSize[0]
        py = y*imageSize[1]
        w = w*imageSize[0]
        h = h*imageSize[1]
        px -= w/2 + w*margin
        py -= h/2 + h*margin
        w *= (1+2*margin)
        h *= (1+2*margin)
        px = int(max(px, 0))
        py = int(max(py, 0))
        w = int(min(w, imageSize[0]-px))
        h = int(min(h, imageSize[1]-py))
        return (px,py,w,h)

    def mergeBoxes(self, boxes, segmentationModelInputSize):
        """Reduces a set of boxes, by merging those which overlap

        Parameters
        ----------
        boxes : [(x,y,w,h)]
            The set of boxes to reduce *in place*
        segmentationModelInputSize : (w, h)
            The reduced boxes will all be smaller than the segmentation model
            input size (if none was already bigger)

        Returns
        -------
        The set of boxes, reduced
        """
        def overlap(b1, b2):
            return b1[0] < b2[0]+b2[2] and b1[0]+b1[2] > b2[0] and b1[1] < b2[1]+b2[3] and b1[1]+b1[3] > b2[1]
        def merge(b1, b2):
            x1,y1,w1,h1 = b1
            x2,y2,w2,h2 = b2
            x = min(x1, x2)
            y = min(y1, y2)
            w = max(x1+w1, x2+w2) - x
            h = max(y1+h1, y2+h2) - y
            return (x, y, w, h)
        def area(b):
            return b[2]*b[3]
        def tryMerge(mergedBoxes, segmentationModelInputSize):
            for i, b1 in enumerate(mergedBoxes):
                for j, b2 in enumerate(mergedBoxes[:i]):
                    if overlap(b1, b2):
                        merged = merge(b1, b2)
                        if area(merged) < (area(b1)+area(b2)) * self.ACCEPTABLE_AREA_LOSS_FACTOR:
                            mergedBoxes[i] = merged
                            del mergedBoxes[j]
                            return True
            return False

        while tryMerge(boxes, segmentationModelInputSize):
            pass

        return boxes

    def placeBoxesOnQuad(self, boxeSizes, margin=20):
        """Arranges a set of boxes on the smallest quad possible

        Parameters
        ----------
        boxeSizes : [(w : int, h : int)]
            The set of boxes to arrange
        margin : int
            The margin to apply between each pair of boxes

        Returns
        -------
        ([(x : int, y : int)], (w : int, h : int))
            The positions of the boxes and the size of the quad
        """
        widths  = list(map(lambda box: box[0], boxeSizes))
        heights = list(map(lambda box: box[1], boxeSizes))
        packedCoords = rpack.pack(zip((w+margin for w in widths), (h+margin for h in heights)))
        packedQuadSize = (max(x+w for (x,y),w in zip(packedCoords, widths)), max(y+h for (x,y),h in zip(packedCoords, heights)))
        return packedCoords, packedQuadSize


def blurPreinit(config):
    """Initializes the blur module.

    This method loads the necessary segmentation model files and loads the model if
    it can be shared between threads, otherwise the models will be initialized by
    blurPicture().

    Parameters
    ----------
    config : {
        BLUR_STRATEGY : "LEGACY" | "FAST" | "COMPROMISE" | "QUALITATIVE",
        MODELS_FS_URL : str,
        YOLOV6_PATH (optional) : str,
    }
        The app configuration
    """
    global blurStrategy
    strategy = config['BLUR_STRATEGY']

    if blurStrategy is not None and blurStrategy.name == strategy:
        return

    precess.setModelsDir(config['MODELS_FS_URL'])

    if 'OMP_NUM_THREADS' not in os.environ and strategy != "LEGACY":
        # By default, limit the cpu usage of segmentation models,
        # it can be disabled by setting the OMP_NUM_THREADS env var
        # however BLUR_THREADS_LIMIT should be used instead for
        # a similar effect. These lines must be executed *before*
        # importing tensorflow
        limit = f"{2}"
        os.environ["OMP_NUM_THREADS"] = limit
        os.environ['TF_NUM_INTEROP_THREADS'] = limit
        os.environ['TF_NUM_INTRAOP_THREADS'] = limit


    if strategy == 'LEGACY':
        # precise segmenter, no downscale
        from .segmenters import PreciseSegmenter

        precess.getModelFile(PreciseSegmenter.MODEL_NAME, PreciseSegmenter.DOWNLOAD_URL)
        preciseSegmenter = PreciseSegmenter()
        blurStrategy = SimpleSegmentationStrategy(strategy, lambda: preciseSegmenter, maxReduction=1/1) # single model instance
    elif strategy == 'FAST':
        # fast segmenter
        from .segmenters import FastSegmenter

        precess.getModelFile(FastSegmenter.MODEL_NAME, FastSegmenter.DOWNLOAD_URL)
        blurStrategy = SimpleSegmentationStrategy(strategy, lambda: getThreadLocalModel(FastSegmenter)) # thread-local model
    elif strategy == 'COMPROMISE':
        # yolo + fast segmenter
        requireObjectDetectors(config)
        from .segmenters import FastSegmenter
        from .objectdetectors import FastInferer

        precess.getModelFile(FastSegmenter.MODEL_NAME, FastSegmenter.DOWNLOAD_URL)
        precess.getModelFile(FastInferer.WEIGHTS_NAME, FastInferer.WEIGHTS_DOWNLOAD_URL)
        precess.getModelFile(FastInferer.LABELS_NAME, FastInferer.LABELS_DOWNLOAD_URL)
        inferer = FastInferer(image_size=640, confidanceThreshold=.25, iouThreshold=.45)
        blurStrategy = DetectionAndSegmentationStrategy(strategy, lambda: getThreadLocalModel(FastSegmenter), lambda: inferer) # thread-local model
    elif strategy == 'QUALITATIVE':
        # yolo + precise segmenter
        requireObjectDetectors(config)
        from .segmenters import PreciseSegmenter
        from .objectdetectors import FastInferer

        precess.getModelFile(PreciseSegmenter.MODEL_NAME, PreciseSegmenter.DOWNLOAD_URL)
        precess.getModelFile(FastInferer.WEIGHTS_NAME, FastInferer.WEIGHTS_DOWNLOAD_URL)
        precess.getModelFile(FastInferer.LABELS_NAME, FastInferer.LABELS_DOWNLOAD_URL)
        preciseSegmenter = PreciseSegmenter()
        inferer = FastInferer(image_size=640, confidanceThreshold=.25, iouThreshold=.45)
        blurStrategy = DetectionAndSegmentationStrategy(strategy, lambda: preciseSegmenter, lambda: inferer) # single model instance
    else:
        raise RuntimeError("Unknown blurring strategy:", strategy) # should have been detected earlier


def getBlurMask(picture):
    """Generates the blur mask using current strategy for a given picture

    blurPreinit() must have been called before this method.

    Parameters
    ----------
    picture : PIL.Image
		Picture file

    Returns
    -------
    PIL.Image
        the blurred image (512px wide, black & white with white being part to blur)
    """
    global blurStrategy
    if blurStrategy is None:
        raise Exception("Blur model was not initialized, please call blur.blurPreinit() method before trying to process pictures")

    return blurStrategy.computeBlurMask(picture).convert("1").resize((512, int(picture.height*512/picture.width)))


def blurPicture(picture, mask):
    """Blurs a single picture using a given mask and returns blurred version.

    Parameters
    ----------
    picture : PIL.Image
		Picture file
    mask : PIL.Image
		Blur mask to apply over image

    Returns
    -------
    PIL.Image
        the blurred image
    """
    if False: # Devtools: replace the blur by a red filter to clearly see what regions of the image are blured
        blurredPicture = Image.new("RGBA", picture.size, (255,0,0,255))
    else:
        blurredPicture = picture.filter(ImageFilter.GaussianBlur(radius=30))

    # Convert 2-colors mask into RGBA
    mask = mask.convert("RGBA")
    newMask = []
    for item in mask.getdata():
        if item[:3] == (0,0,0):
            newMask.append((0,0,0,0))
        else:
            newMask.append(item)
    mask.putdata(newMask)

    partiallyBlurredPicture = picture.copy()
    partiallyBlurredPicture.paste(blurredPicture, (0, 0), mask.resize(picture.size))
    return partiallyBlurredPicture
