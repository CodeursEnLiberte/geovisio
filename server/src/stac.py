import psycopg
import re
import json
from uuid import UUID
from psycopg.rows import dict_row
from psycopg.types.json import Jsonb
from dateutil.parser import parse as dateparser
from flask import Blueprint, current_app, request
from . import errors, pictures


CONFORMANCE_LIST = [
	"https://api.stacspec.org/v1.0.0-rc.1/core"
]
STAC_VERSION = "1.0.0"
STAC_PREFIX = "/api"

bp = Blueprint('stac', __name__, url_prefix=STAC_PREFIX)


def getURL(request, current_app, route=""):
	"""Get the HTTP URL from Flask environment and config

	Parameters
	----------
	request
	current_app
	route : str (optional)
		The API route (will be append to URL)

	Returns
	-------
	url : str
		The computed URL
	"""
	return request.scheme + "://" + current_app.config['SERVER_NAME'] + route


@bp.route('/')
def getLanding():
	"""Retrieves API resources list
	---
		responses:
			200:
				description: the Catalog listing resources available in this API
				schema:
					$ref: 'https://schemas.stacspec.org/v1.0.0/catalog-spec/json-schema/catalog.json'
	"""

	return {
		"stac_version": STAC_VERSION,
		"id": "geovisio@" + current_app.config['SERVER_NAME'],
		"title": "GeoVisio STAC API",
		"description": "This catalog list all geolocated pictures available in this GeoVisio instance",
		"type": "Catalog",
		"conformsTo" : CONFORMANCE_LIST,
		"links": [
			{
				"rel": "self",
				"type": "application/json",
				"href": getURL(request, current_app, STAC_PREFIX)
			},
			{
				"rel": "root",
				"type": "application/json",
				"href": getURL(request, current_app, STAC_PREFIX)
			},
			{
				"rel": "service-desc",
				"type": "application/vnd.oai.openapi+json;version=3.0",
				"href": getURL(request, current_app, "/apispec_1.json")
			},
			{
				"rel": "service-doc",
				"type": "text/html",
				"href": getURL(request, current_app, "/apidocs")
			},
			{
				"rel": "conformance",
				"type": "application/json",
				"href": getURL(request, current_app, STAC_PREFIX + "/conformance")
			},
			{
				"rel": "data",
				"type": "application/json",
				"href": getURL(request, current_app, STAC_PREFIX + "/collections")
			},
			{
				"rel": "search",
				"type": "application/geo+json",
				"href": getURL(request, current_app, STAC_PREFIX + "/search")
			}
		]
	}


@bp.route('/conformance')
def getConformance():
	"""List definitions this API conforms to
	---
		responses:
			200:
				description: the list of definitions this API conforms to
	"""

	return { "conformsTo": CONFORMANCE_LIST }


def dbSequenceToStacCollection(dbSeq, request, current_app):
	"""Transforms a sequence extracted from database into a STAC Collection

	Parameters
	----------
	dbSeq : dict
		A row from sequences table in database (with id, name, minx, miny, maxx, maxy, mints, maxts fields)
	request
	current_app

	Returns
	-------
	object
		The equivalent in STAC Collection format
	"""

	return {
		"type": "Collection",
		"stac_version": STAC_VERSION,
		"id": str(dbSeq["id"]),
		"title": str(dbSeq["name"]),
		"description": "A sequence of geolocated pictures",
		"keywords": ["pictures", str(dbSeq["name"])],
		"license": "proprietary", # TODO Handle a proper licensing system
		"extent": {
			"spatial": { "bbox": [ [dbSeq["minx"], dbSeq["miny"], dbSeq["maxx"], dbSeq["maxy"]] ] },
			"temporal": { "interval": [[ dbSeq["mints"].isoformat(), dbSeq["maxts"].isoformat() ]] }
		},
		"links": [
			{
			  "rel": "items",
			  "type": "application/geo+json",
			  "href": getURL(request, current_app, STAC_PREFIX + "/collections/" + str(dbSeq["id"]) + "/items")
			},
			{
			  "rel": "parent",
			  "type": "application/json",
			  "href": getURL(request, current_app, STAC_PREFIX)
			},
			{
			  "rel": "root",
			  "type": "application/json",
			  "href": getURL(request, current_app, STAC_PREFIX)
			},
			{
			  "rel": "self",
			  "type": "application/json",
			  "href": getURL(request, current_app, STAC_PREFIX + "/collections/" + str(dbSeq["id"]))
			}
		]
	}


@bp.route('/collections')
def getAllCollections():
	"""List available collections
	---
		responses:
			200:
				description: the list of available collections
				schema:
					$ref: '#/definitions/Collections'
	"""

	with psycopg.connect(current_app.config['DB_URL'], row_factory=dict_row) as conn:
		with conn.cursor() as cursor:
			records = cursor.execute("""
				SELECT
					s.id, s.folder_path AS name,
					ST_XMin(s.geom) AS minx,
					ST_YMin(s.geom) AS miny,
					ST_XMax(s.geom) AS maxx,
					ST_YMax(s.geom) AS maxy,
					a.mints,
					a.maxts
				FROM sequences s
				JOIN (
					SELECT
						sp.seq_id,
						MIN(ts) as mints,
						MAX(ts) as maxts
					FROM pictures p
					JOIN sequences_pictures sp ON sp.pic_id = p.id
					GROUP BY sp.seq_id
				) a ON a.seq_id = s.id
			""")

			collections = [ dbSequenceToStacCollection(dbSeq, request, current_app) for dbSeq in records ]

			return {
				"collections": collections,
				"links": [
					{
					  "rel": "root",
					  "type": "application/json",
					  "href": getURL(request, current_app, STAC_PREFIX)
					},
					{
					  "rel": "self",
					  "type": "application/json",
					  "href": getURL(request, current_app, STAC_PREFIX + "/collections")
					}
				]
			}

		raise errors.InvalidAPIUsage("Failed to connect to database", status_code=500)

	raise errors.InvalidAPIUsage("Failed to connect to database", status_code=500)


@bp.route('/collections/<collectionId>')
def getCollection(collectionId):
	"""Retrieve metadata of a single collection
	---
		parameters:
			- name: collectionId
			  in: path
			  type: string
			  description: ID of collection to retrieve
		responses:
			200:
				description: the collection metadata
				schema:
					$ref: 'https://schemas.stacspec.org/v1.0.0/collection-spec/json-schema/collection.json'
	"""

	with psycopg.connect(current_app.config['DB_URL'], row_factory=dict_row) as conn:
		with conn.cursor() as cursor:
			record = cursor.execute("""
				SELECT
					s.id, s.folder_path AS name,
					ST_XMin(s.geom) AS minx,
					ST_YMin(s.geom) AS miny,
					ST_XMax(s.geom) AS maxx,
					ST_YMax(s.geom) AS maxy,
					a.mints,
					a.maxts
				FROM sequences s, (
					SELECT
						MIN(ts) as mints,
						MAX(ts) as maxts
					FROM pictures p
					JOIN sequences_pictures sp ON sp.seq_id = %(id)s AND sp.pic_id = p.id
				) a
				WHERE s.id = %(id)s
			""", { "id": collectionId }).fetchone()

			if record is None:
				raise errors.InvalidAPIUsage("Collection doesn't exist", status_code=404)

			return dbSequenceToStacCollection(record, request, current_app)

		raise errors.InvalidAPIUsage("Failed to connect to database", status_code=500)

	raise errors.InvalidAPIUsage("Failed to connect to database", status_code=500)


def dbPictureToStacItem(seqId, dbPic, request, current_app):
	"""Transforms a picture extracted from database into a STAC Item

	Parameters
	----------
	seqId : uuid
		Associated sequence ID
	dbSeq : dict
		A row from pictures table in database (with id, geojson, ts, heading, cols, rows, width, height, prevpic, nextpic, prevpicgeojson, nextpicgeojson fields)
	request
	current_app

	Returns
	-------
	object
		The equivalent in STAC Item format
	"""

	item = {
		"type": "Feature",
		"stac_version": STAC_VERSION,
		"stac_extensions": [
			"https://stac-extensions.github.io/view/v1.0.0/schema.json", # "view:" fields
			"https://raw.githubusercontent.com/stac-extensions/perspective-imagery/main/json-schema/schema.json" # "pers:" fields
		],
		"id": str(dbPic["id"]),
		"geometry": dbPic["geojson"],
		"bbox": dbPic["geojson"]["coordinates"] + dbPic["geojson"]["coordinates"],
		"properties": {
			"datetime": dbPic["ts"].isoformat(),
			"license": "proprietary", # TODO Proper handling of licenses
			"view:azimuth": dbPic["heading"],
			"pers:interior_orientation": {
				"camera_manufacturer": dbPic["metadata"]["make"],
				"camera_model": dbPic["metadata"]["model"],
				"focal_length": dbPic["metadata"]["focal_length"],
				"field_of_view": dbPic["metadata"]["field_of_view"] if "field_of_view" in dbPic["metadata"] else None
			}
		},
		"links": [
			{
				"rel": "self",
				"type": "application/geo+json",
				"href": getURL(request, current_app, STAC_PREFIX + "/collections/" + str(seqId) + "/items/" + str(dbPic["id"]))
			},
			{
				"rel": "collection",
				"type": "application/json",
				"href": getURL(request, current_app, STAC_PREFIX + "/collections/" + str(seqId))
			}
		],
		"assets": {
			"hd_webp": {
				"title": "HD picture",
				"description": "Highest resolution available of this picture",
				"roles": [ "data" ],
				"type": "image/webp",
				"href": getURL(request, current_app, "/api/pictures/" + str(dbPic["id"]) + "/hd.webp")
			},
			"sd_webp": {
				"title": "SD picture",
				"description": "Picture in standard definition (fixed width of 2048px)",
				"roles": [ "visual" ],
				"type": "image/webp",
				"href": getURL(request, current_app, "/api/pictures/" + str(dbPic["id"]) + "/sd.webp")
			},
			"thumb_webp": {
				"title": "Thumbnail",
				"description": "Picture in low definition (fixed width of 500px)",
				"roles": [ "thumbnail" ],
				"type": "image/webp",
				"href": getURL(request, current_app, "/api/pictures/" + str(dbPic["id"]) + "/thumb.webp")
			},
			"hd": {
				"title": "HD picture",
				"description": "Highest resolution available of this picture",
				"roles": [ "data" ],
				"type": "image/jpeg",
				"href": getURL(request, current_app, "/api/pictures/" + str(dbPic["id"]) + "/hd.jpg")
			},
			"sd": {
				"title": "SD picture",
				"description": "Picture in standard definition (fixed width of 2048px)",
				"roles": [ "visual" ],
				"type": "image/jpeg",
				"href": getURL(request, current_app, "/api/pictures/" + str(dbPic["id"]) + "/sd.jpg")
			},
			"thumb": {
				"title": "Thumbnail",
				"description": "Picture in low definition (fixed width of 500px)",
				"roles": [ "thumbnail" ],
				"type": "image/jpeg",
				"href": getURL(request, current_app, "/api/pictures/" + str(dbPic["id"]) + "/thumb.jpg")
			}
		},
		"collection": str(seqId)
	}

	# Next / previous links if any
	if "nextpic" in dbPic and dbPic["nextpic"] is not None:
		item["links"].append({
			"rel": "next",
			"type": "application/geo+json",
			"geometry": dbPic["nextpicgeojson"],
			"id": dbPic["nextpic"],
			"href": getURL(request, current_app, STAC_PREFIX + "/collections/" + str(seqId) + "/items/" + str(dbPic["nextpic"]))
		})

	if "prevpic" in dbPic and dbPic["prevpic"] is not None:
		item["links"].append({
			"rel": "prev",
			"type": "application/geo+json",
			"geometry": dbPic["prevpicgeojson"],
			"id": dbPic["prevpic"],
			"href": getURL(request, current_app, STAC_PREFIX + "/collections/" + str(seqId) + "/items/" + str(dbPic["prevpic"]))
		})

	#
	# Picture type-specific properties
	#

	# Equirectangular
	if dbPic["metadata"]["type"] == "equirectangular":
		item["stac_extensions"].append("https://stac-extensions.github.io/tiled-assets/v1.0.0/schema.json") # "tiles:" fields

		item["properties"]["tiles:tile_matrix_sets"] = {
			"geovisio": {
				"type": "TileMatrixSetType",
				"title": "GeoVisio tile matrix for picture " + str(dbPic["id"]),
				"identifier": "geovisio-" + str(dbPic["id"]),
				"tileMatrix": [
					{
						"type": "TileMatrixType",
						"identifier": "0",
						"scaleDenominator": 1,
						"topLeftCorner": [ 0,0 ],
						"tileWidth": dbPic["metadata"]["width"] / dbPic["metadata"]["cols"],
						"tileHeight": dbPic["metadata"]["height"] / dbPic["metadata"]["rows"],
						"matrixWidth": dbPic["metadata"]["cols"],
						"matrixHeight": dbPic["metadata"]["rows"]
					}
				]
			}
		}

		item["asset_templates"] = {
			"tiles_webp": {
				"title": "HD tiled picture",
				"description": "Highest resolution available of this picture, as tiles",
				"roles": [ "data" ],
				"type": "image/webp",
				"href": getURL(request, current_app, "/api/pictures/" + str(dbPic["id"]) + "/tiled/{TileCol}_{TileRow}.webp")
			},
			"tiles": {
				"title": "HD tiled picture",
				"description": "Highest resolution available of this picture, as tiles",
				"roles": [ "data" ],
				"type": "image/jpeg",
				"href": getURL(request, current_app, "/api/pictures/" + str(dbPic["id"]) + "/tiled/{TileCol}_{TileRow}.jpg")
			}
		}

	return item


@bp.route('/collections/<collectionId>/items')
def getCollectionItems(collectionId):
	"""List items of a single collection
	---
		parameters:
			- name: collectionId
			  in: path
			  type: string
			  description: ID of collection to retrieve
		responses:
			200:
				description: the items list
				schema:
					$ref: 'https://geojson.org/schema/FeatureCollection.json'
	"""

	with psycopg.connect(current_app.config['DB_URL'], row_factory=dict_row) as conn:
		with conn.cursor() as cursor:
			records = cursor.execute("""
				SELECT
					p.id, p.ts, p.heading, p.metadata,
					ST_AsGeoJSON(p.geom)::json AS geojson,
					CASE WHEN LAG(p.status) OVER othpics = 'ready' THEN LAG(p.id) OVER othpics END AS prevpic,
					CASE WHEN LAG(p.status) OVER othpics = 'ready' THEN ST_AsGeoJSON(LAG(p.geom) OVER othpics)::json END AS prevpicgeojson,
					CASE WHEN LEAD(p.status) OVER othpics = 'ready' THEN LEAD(p.id) OVER othpics END AS nextpic,
					CASE WHEN LEAD(p.status) OVER othpics = 'ready' THEN ST_AsGeoJSON(LEAD(p.geom) OVER othpics)::json END AS nextpicgeojson
				FROM sequences_pictures sp
				JOIN pictures p ON sp.pic_id = p.id
				WHERE sp.seq_id = %(seq)s AND p.status = 'ready'
				WINDOW othpics AS (PARTITION BY sp.seq_id ORDER BY sp.rank)
				ORDER BY rank
			""", { "seq": collectionId })

			items = [ dbPictureToStacItem(
				collectionId,
				dbPic,
				request,
				current_app
			) for dbPic in records ]

			return {
				"type": "FeatureCollection",
				"features": items,
				"links": [
					{
						"rel": "self",
						"type": "application/geo+json",
						"href": getURL(request, current_app, STAC_PREFIX + "/collections/" + collectionId + "/items")
					}
				]
			}

		raise errors.InvalidAPIUsage("Failed to connect to database", status_code=500)

	raise errors.InvalidAPIUsage("Failed to connect to database", status_code=500)


@bp.route('/collections/<collectionId>/items/<itemId>')
def getCollectionItem(collectionId, itemId):
	"""Get a single item from a collection
	---
		parameters:
			- name: collectionId
			  in: path
			  type: string
			  description: ID of collection to retrieve
			- name: itemId
			  in: path
			  type: string
			  description: ID of item to retrieve
		responses:
			200:
				description: the wanted item
				schema:
					$ref: 'https://schemas.stacspec.org/v1.0.0/item-spec/json-schema/item.json'
	"""

	with psycopg.connect(current_app.config['DB_URL'], row_factory=dict_row) as conn:
		with conn.cursor() as cursor:
			# Get rank + position of wanted picture
			record = cursor.execute("""
				SELECT p.id, sp.rank, ST_AsGeoJSON(p.geom)::json AS geojson, p.heading, p.ts, p.metadata
				FROM sequences_pictures sp, pictures p
				WHERE sp.seq_id = %(seq)s
					AND sp.pic_id = %(pic)s
					AND p.id = %(pic)s
					AND p.status = 'ready'
			""", { "seq": collectionId, "pic": itemId }).fetchone()

			if record is None:
				raise errors.InvalidAPIUsage("Item doesn't exist", status_code=404)

			# Get next/prev pic in sequence position and ID
			for rank in [record["rank"]-1, record["rank"]+1]:
				record2 = cursor.execute("""
					SELECT sp.pic_id AS id, ST_AsGeoJSON(p.geom)::json AS geojson
					FROM sequences_pictures sp
					JOIN pictures p on sp.pic_id = p.id
					WHERE sp.seq_id = %(seq)s
						AND sp.rank = %(rank)s
						AND p.status = 'ready'
				""", { "seq": collectionId, "rank": rank }).fetchone()

				if record2:
					if rank == record["rank"]-1:
						record["prevpic"] = record2["id"]
						record["prevpicgeojson"] = record2["geojson"]
					elif rank == record["rank"]+1:
						record["nextpic"] = record2["id"]
						record["nextpicgeojson"] = record2["geojson"]

			return dbPictureToStacItem(
				collectionId,
				record,
				request,
				current_app
			)

		raise errors.InvalidAPIUsage("Failed to connect to database", status_code=500)

	raise errors.InvalidAPIUsage("Failed to connect to database", status_code=500)


@bp.route('/search')
def searchItems():
	"""Search through all available items
	---
		parameters:
			- name: limit
			  in: query
			  type: number
			  description: The maximum number of results to return
			- name: bbox
			  in: query
			  type: [number]
			  description: Requested bounding box
			- name: datetime
			  in: query
			  type: string
			  description: Single date+time, or a range ('/' separator), formatted to RFC 3339, section 5.6. Use double dots .. for open date ranges.
			- name: intersects
			  in: query
			  type: string
			  description: Searches items by performing intersection between their geometry and provided GeoJSON geometry
			- name: ids
			  in: query
			  type: [string]
			  description: Array of Item ids to return
			- name: collections
			  in: query
			  type: [string]
			  description: Array of one or more Collection IDs that each matching Item must be in
		responses:
			200:
				description: the items list
				schema:
					$ref: 'https://geojson.org/schema/FeatureCollection.json'
	"""

	sqlWhere = [ "p.status = 'ready'" ]
	sqlParams = {}

	#
	# Parameters parsing and verification
	#

	# Limit
	if request.args.get("limit") is not None:
		limit = request.args.get("limit", type=int)
		if limit is None or limit < 1 or limit > 10000:
			raise errors.InvalidAPIUsage("Parameter limit must be either empty or a number between 1 and 10000", status_code=400)
		else:
			sqlParams["limit"] = limit
	else:
		sqlParams["limit"] = 10000

	# Bounding box
	if request.args.get("bbox") is not None:
		try:
			bbox = [float(n) for n in request.args.get("bbox")[1:-1].split(",")]
			if len(bbox) != 4 or not all(isinstance(x, float) for x in bbox):
				raise ValueError()
			elif bbox[0] < -180 or bbox[0] > 180 or bbox[1] < -90 or bbox[1] > 90 or bbox[2] < -180 or bbox[2] > 180 or bbox[3] < -90 or bbox[3] > 90:
				raise errors.InvalidAPIUsage("Parameter bbox must contain valid longitude (-180 to 180) and latitude (-90 to 90) values", status_code=400)
			else:
				sqlWhere.append("p.geom && ST_MakeEnvelope(%(minx)s, %(miny)s, %(maxx)s, %(maxy)s, 4326)")
				sqlParams["minx"] = bbox[0]
				sqlParams["miny"] = bbox[1]
				sqlParams["maxx"] = bbox[2]
				sqlParams["maxy"] = bbox[3]
		except ValueError:
			raise errors.InvalidAPIUsage("Parameter bbox must be in format [minX, minY, maxX, maxY]", status_code=400)

	# Datetime
	if request.args.get("datetime") is not None:
		try:
			dates = request.args.get("datetime").split("/")

			if len(dates) == 1:
				date = dateparser(dates[0])
				sqlWhere.append("p.ts = %(ts)s::timestamp with time zone")
				sqlParams["ts"] = date

			elif len(dates) == 2:
				# Check if interval is closed or open-ended
				if dates[0] == "..":
					date = dateparser(dates[1])
					sqlWhere.append("p.ts <= %(ts)s::timestamp with time zone")
					sqlParams["ts"] = date
				elif dates[1] == "..":
					date = dateparser(dates[0])
					sqlWhere.append("p.ts >= %(ts)s::timestamp with time zone")
					sqlParams["ts"] = date
				else:
					date0 = dateparser(dates[0])
					date1 = dateparser(dates[1])
					sqlWhere.append("p.ts >= %(mints)s::timestamp with time zone")
					sqlWhere.append("p.ts <= %(maxts)s::timestamp with time zone")
					sqlParams["mints"] = date0
					sqlParams["maxts"] = date1
			else:
				raise errors.InvalidAPIUsage("Parameter datetime should contain one or two dates", status_code=400)
		except:
			raise errors.InvalidAPIUsage("Parameter datetime contains an invalid date definition", status_code=400)

	# Intersects
	if request.args.get("intersects") is not None:
		try:
			intersects = json.loads(request.args.get("intersects"))
			if intersects['type'] == "Point":
				sqlWhere.append("ST_DWithin(p.geom::geography, ST_GeomFromGeoJSON(%(geom)s)::geography, 0.01)")
			else:
				sqlWhere.append("p.geom && ST_GeomFromGeoJSON(%(geom)s)")
				sqlWhere.append("ST_Intersects(p.geom, ST_GeomFromGeoJSON(%(geom)s))")
			sqlParams["geom"] = Jsonb(intersects)
		except:
			raise errors.InvalidAPIUsage("Parameter intersects should contain a valid GeoJSON Geometry (not a Feature)", status_code=400)

	# Ids
	if request.args.get("ids") is not None:
		try:
			sqlWhere.append("p.id = ANY(%(ids)s)")
			sqlParams["ids"] = [ UUID(j) for j in json.loads(request.args.get("ids")) ]
		except:
			raise errors.InvalidAPIUsage("Parameter ids should be a JSON array of strings", status_code=400)

	# Collections
	if request.args.get("collections") is not None:
		try:
			sqlWhere.append("sp.seq_id = ANY(%(collections)s)")
			sqlParams["collections"] = [ UUID(j) for j in json.loads(request.args.get("collections")) ]
		except:
			raise errors.InvalidAPIUsage("Parameter collections should be a JSON array of strings", status_code=400)


	#
	# Database query
	#

	with psycopg.connect(current_app.config['DB_URL'], row_factory=dict_row, options="-c statement_timeout=30000") as conn:
		with conn.cursor() as cursor:
			records = cursor.execute("""
				SELECT
					p.id, p.ts, p.heading, p.metadata,
					ST_AsGeoJSON(p.geom)::json AS geojson,
					sp.seq_id,
					spl.prevpic, spl.prevpicgeojson, spl.nextpic, spl.nextpicgeojson
				FROM pictures p
				LEFT JOIN sequences_pictures sp ON p.id = sp.pic_id
				LEFT JOIN (
					SELECT
						p.id,
						LAG(p.id) OVER othpics AS prevpic,
						ST_AsGeoJSON(LAG(p.geom) OVER othpics)::json AS prevpicgeojson,
						LEAD(p.id) OVER othpics AS nextpic,
						ST_AsGeoJSON(LEAD(p.geom) OVER othpics)::json AS nextpicgeojson
					FROM pictures p
					JOIN sequences_pictures sp ON p.id = sp.pic_id
					WHERE p.status = 'ready'
					WINDOW othpics AS (PARTITION BY sp.seq_id ORDER BY sp.rank)
				) spl ON p.id = spl.id
				WHERE
				""" + " AND ".join(sqlWhere) + """
				LIMIT %(limit)s
			""", sqlParams)

			items = [ dbPictureToStacItem(
				str(dbPic["seq_id"]),
				dbPic,
				request,
				current_app
			) for dbPic in records ]

			return {
				"type": "FeatureCollection",
				"features": items,
				"links": []
			}

		raise errors.InvalidAPIUsage("Failed to connect to database", status_code=500)

	raise errors.InvalidAPIUsage("Failed to connect to database", status_code=500)
