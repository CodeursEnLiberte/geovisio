# GeoVisio / Server

Server is the component handling hosting of pictures. It analyzes and serves your geolocated pictures through an API.

## Dependencies

GeoVisio best runs with a Linux server and needs these components:

- Python __3.9__, higher versions are currently unsupported by some blurring algorithms
- A [PostgreSQL](https://www.postgresql.org/) 10+ database with [PostGIS](https://postgis.net/) 3+ extension
- Specific dependencies according to blur strategy used, see [blur documentation](./src/blur/README.md) for more info

## Setup

### Database configuration

If you're starting with a fresh PostgreSQL install (and not sure about what to do), you need to change these parameters in order to have GeoVisio working:

- Create a new database (with __UTF-8 encoding__) using this command:

```bash
sudo su - postgres -c "psql -c \"CREATE DATABASE geovisio2 ENCODING 'UTF-8' TEMPLATE template0\""
```

- Change the `listen_adresses` parameter in `postgresql.conf` to be `'*'`
- Add in `pg_hba.conf` file the following line (considering your username is `postgres` and database `geovisio`)

```
host	geovisio	postgres	172.17.0.0/24	trust
```

Once your database is ready, you can go through these steps

### Basic install

```bash
# Set your config through environment variables
export DB_URL="postgres://postgres@172.17.0.1/geovisio" # May change according to your config
export FS_URL="osfs:///my/pic/dir" # You have to change the /my/pic/dir path in the command to your actual pictures folder

# Enable Python environment
python3 -m venv env
source ./env/bin/activate
pip install -r requirements.txt
pip install -r requirements-blur.txt # Only if you want to enable pictures blurring
pip install -r requirements-dev.txt # Only for dev or testing purposes

# Command to run pictures processing
# It will look through your pictures folder and create appropriate metadata in database
FLASK_APP="src" flask process-sequences

# Command to start API
FLASK_APP="src" flask run
```

### Alternative: Docker install

```bash
export DB_URL="postgres://postgres@172.17.0.1/geovisio" # May change according to your config

# Retrieve the stable Docker image
# An image with latest developments is also available: panieravide/geovisio:develop
docker pull panieravide/geovisio

# Command to run pictures processing (metadata parsing, thumbnails/tiles/blurring processing)
# This can take several minutes according to your pictures folder size
# You have to change the /my/pic/dir path in the command to your actual pictures folder
docker run \
	-e DB_URL \
	-v /my/pic/dir:/data/360app \
	panieravide/geovisio \
	process-sequences

# Command to start API
docker run --rm --name=geovisio \
	-p 5000:5000 \
	-e DB_URL \
	-v /my/pic/dir:/data/360app \
	panieravide/geovisio \
	api
```

### Testing

Various unit tests are available to ensure the server is well working. You can run them by following these steps:

- In your `config.py` file, add a `TEST_DB_URL` parameter, which follows the `DB_URL` parameter format, so you can use a dedicated database for testing
- Run `pytest` command

Unit tests are available mainly in `/server/tests/` folder, some simpler tests are directly written as [doctests](https://docs.python.org/3/library/doctest.html) in their respective source files (in `/server/src/`).

### Deploy in production

When deploying in a production environment, you should at some point plan to __regularly run__ these operations:

- PostgreSQL database REINDEX / VACUUM (~ once per day/week)
- Process sequences command (~ every 5 minutes)

You may need to use `SERVER_NAME` parameter and `ssl-api` command for launching an API if your GeoVisio server is behind a proxy.

If you plan to use GeoVisio __blurring system__, you may want to choose a specific algorithm according to your needs. We highly recommend to read the [blur documentation](./src/blur/README.md). If you use blur with __Docker__, you may want to map container folder `/data/360models` to an host folder where there is enough space to store blurring models.

### Updating a production instance database schema

when updating a running Geovisio instance, if a database schema update is needed, the app won't be able to be run without applying the database schema update process. As a rule of thumb, this process should be always run before updating a running geovisio instance.

```bash
FLASK_APP="src" flask db upgrade
```

You can alternatively run its docker counterpart
```bash
docker run --rm \
	-e DB_URL \
	-v /my/pic/dir:/data/360app \
	panieravide/geovisio \
	db-upgrade
```

Note: the migrations are technically handled by [yoyo-migrations](https://ollycope.com/software/yoyo/latest/).

For advanced schema handling (like listing the migrations, replaying a migration, ...) you can use all yoyo's command.

eg. to list all the migrations:

```bash
yoyo list --database postgresql+psycopg://user:pwd@host:port/database
```

Note: the database connection string should use `postgresql+psycopg://` in order to force yoyo to use psycopg v3.

#### Rollbacking a migration

If there a problem with a migration you can rollback it with

```bash
FLASK_APP="src" flask db rollback
```

You can also rollback all migrations with:
⚠️ This will reset your database ⚠️

```bash
FLASK_APP="src" flask db rollback --all
```


### Uninstall / cleaning

If you want to uninstall or clean-up GeoVisio files, you can run the following command:

```bash
FLASK_APP="src" flask cleanup
```

This will delete database GeoVisio tables and derivated pictures files (`geovisio_derivates` folder). It will __not delete__ your original pictures files.

You can also run some partial cleaning with the same cleanup command and one of the following options:

```bash
FLASK_APP="src" flask cleanup \
    --database \ # Removes entries from database, but not picture derivates
    --cache \ # Only removes picture derivates, but not blur masks
    --blur-masks # Only removes blur masks
```


## Configuration

Server configuration can be set through different methods:

- By setting `config.py` file (see [example](./config.example.py))
- By using system environment variables

__Mandatory__ parameters are :

- `DB_URL` : [connection string](https://www.postgresql.org/docs/current/libpq-connect.html#LIBPQ-CONNSTRING) to access the PostgreSQL database. You can alternatively use a set of `DB_PORT`, `DB_HOST`, `DB_USERNAME`, `DB_PASSWORD`, `DB_NAME` parameters to configure database access.
- `FS_URL` : file system and directory to use for reading and storing pictures (in [PyFilesystem format](https://docs.pyfilesystem.org/en/latest/openers.html)), for example : `osfs:///path/to/pic/dir` (disk storage), `s3://mybucket/myfolder?endpoint_url=https%3A%2F%2Fs3.fr-par.scw.cloud&region=fr-par` (S3 storage)

__Optional__ parameters are :
- `SERVER_NAME` : publicly visible HTTP API hostname (see [Flask SERVER_NAME parameter](https://flask.palletsprojects.com/en/2.1.x/config/#SERVER_NAME) for details), for example : `myserver.net:3000`
- `BLUR_STRATEGY` : activate blurring for all new picture uploads. See [blur documentation](./src/blur/README.md) for more info.
- `DERIVATES_STRATEGY` : sets if picture derivates versions should be pre-processed (value `PREPROCESS` to generate all derivates during `process-sequences`) or created on-demand at user query (value `ON_DEMAND` by default to generate at first API call needing the picture).
- `BACKEND_MODE` : sets the mode to run GeoVisio into ("api" for web API, "process-sequences" for checking and processing incoming pictures). This is used mainly for Kubernetes / Dockerized deployments to avoid passing arguments to container.
- `MAIN_PAGE` and `VIEWER_PAGE` : changes the template to use for display main and viewer page. This should be the name of an existing HTML template in `/server/src/templates` folder, for example `main.html` or `viewer.html`.
- `WEBP_METHOD` : quality/speed trade-off for WebP encoding of pictures derivates (0=fast, 6=slower-better, 6 by default).


## Usage

### Prepare your pictures

GeoVisio has several prerequisites for a pictures to be accepted, mainly concerning availability of some [EXIF tags](https://en.wikipedia.org/wiki/Exif). A picture __must__ have the following EXIF tags defined:

- GPS coordinates with `GPSLatitude, GPSLatitudeRef, GPSLongitude, GPSLongitudeRef`
- Date, either with `GPSTimeStamp` or `DateTimeOriginal`

The following EXIF tags are recognized and used if defined, but are __optional__:

- Image orientation, either with `GPSImgDirection` or `GPano:PoseHeadingDegrees` (was mandatory in versions <= 1.3.1)
- Milliseconds in date with `SubSecTimeOriginal`
- If picture is 360° / spherical with `GPano:ProjectionType`
- Camera model with `Make, Model`
- Camera focal length (to get precise field of view) with `FocalLength`

Note that GeoVisio now accepts both __360° and classic/flat pictures__ (versions <= 1.2.0 only supported 360° pictures).

### Organize your pictures

For GeoVisio to recognize your pictures and sequences, your instance root folder (`FS_URL` parameter) should contain a list of subfolders (one per sequence), and each subfolder should contain a list of pictures (JPEG files, read in alphabetical order). As an example of tree hierarchy:

- Main folder (according to your `FS_URL` parameter)
  - `my_seq_1` : one sequence folder
    - `my_pic_1.jpg`
    - `my_pic_2.jpg`
    - `my_pic_3.jpg`
    - ...
  - `my_seq_2` : another sequence
    - And its pictures...
  - ...

Once this directory is ready, the `process-sequences` will look through it and parse necessary metadata. Note that original pictures and folder structure __will not be changed__. Although, GeoVisio will add various automatically generated files, all prefixed with `geovisio_` so you can easily clean-up if necessary. Also note that all sequences folder with a name starting with `geovisio_`, `gvs_` or `ignore_` will not be processed, allowing to put aside some sequences you don't want to be processed at some point.

Eventually, if you want to clear database and delete derivate versions of pictures (it __doesn't__ delete original pictures), you can use the `cleanup` command:

```bash
FLASK_APP="src" flask run
```

Also, if you need to re-process one or several sequences (to re-do pictures blurring with another strategy for example, or after fixing metadata of some pictures), you can use the `redo-sequences` command followed by sequences folder names:

```bash
FLASK_APP="src" flask redo-sequences <SEQUENCE_FOLDERNAME_1> <SEQUENCE_FOLDERNAME_2> ...
```

Since version 1.4.0, you can import pictures without heading metadata. By default, heading is computed based on sequence movement path (looking in front), but you can edit manually after import using this command:

```bash
FLASK_APP="src" flask set-sequences-heading --value <DEGREES_ROTATION_FROM_FORWARD> --overwrite <SEQUENCE_FOLDERNAME_1> <SEQUENCE_FOLDERNAME_2> ...
```

#### Mock data for development

If you want to feed Geovisio with mock data to test its performance, you can run this command:

```bash
FLASK_APP="src" BLUR_STRATEGY=DISABLE flask fill-mock-data --base-image-path=<path to the base image to duplicate>
```

This will create a number of sequences, and for each sequence duplicate the base image and update its exif metadata to change the position and its date.

### API

API routes are documented at [localhost:5000/apidocs](http://localhost:5000/apidocs). It allows you to retrieve sequence and picture metadata, and also access to raw or edited versions of your pictures. API is also compliant with [STAC API scheme](https://github.com/radiantearth/stac-api-spec) through `/stac/` route.

## Development

### Adding a new migration

To create a new migration, use [yoyo-migrations](https://ollycope.com/software/yoyo/latest/).

The `yoyo` binary should be available if the python dependencies are installed.

The prefered way to create migration is to use raw sql, but if needed a python migration script can be added.

```bash
yoyo new -m "<a migration name>" --sql
```

(remove the `--sql` to generate a python migration).

This will open an editor to a migration in `./migrations`.

Once saved, for sql migrations, always provide another file named like the initial migration but with a `.rollback.sql` suffix, with the associated rollback actions.

Note: each migration is run inside a transaction.
