from flask import json
import pytest
import psycopg
import re
from pystac import Catalog, Collection, ItemCollection, Item
from src import create_app, stac
from datetime import datetime
from uuid import UUID
from . import conftest
from urllib.parse import urlencode


# Mock classes for easier getURL / getStacURL testing
class Request(object):
	def __init__(self, scheme):
		self.scheme = scheme

class App(object):
	def __init__(self, server):
		self.config = {"SERVER_NAME": server }


@pytest.mark.parametrize(('scheme', 'server', 'route', 'res'), (
	('https', 'geovisio.fr', None, 'https://geovisio.fr'),
	('http', 'geovisio.fr', None, 'http://geovisio.fr'),
	('https', 'geovisio.fr', '/api/api', 'https://geovisio.fr/api/api'),
))
def test_getURL(scheme, server, route, res):
	if route is None:
		assert stac.getURL(Request(scheme), App(server)) == res
	else:
		assert stac.getURL(Request(scheme), App(server), route) == res


def test_landing(client):
	response = client.get('/api/')
	data = json.loads(response.get_data(as_text=True))

	assert response.status_code == 200
	assert data['type'] == 'Catalog'
	ctl = Catalog.from_dict(data)
	assert len(ctl.links) > 0
	assert ctl.title == "GeoVisio STAC API"
	assert ctl.id == "geovisio@localhost:5000"


def test_conformance(client):
	response = client.get('/api/conformance')
	data = json.loads(response.get_data(as_text=True))

	assert response.status_code == 200
	assert data['conformsTo'] == stac.CONFORMANCE_LIST


def test_dbSequenceToStacCollection():
	dbSeq = {
		"id": UUID('{12345678-1234-5678-1234-567812345678}'),
		"name": "Test sequence",
		"minx": -1.0,
		"maxx": 1.0,
		"miny": -2.0,
		"maxy": 2.0,
		"mints": datetime.fromisoformat("2020-01-01T12:50:37"),
		"maxts": datetime.fromisoformat("2020-01-01T13:30:42")
	}

	res = stac.dbSequenceToStacCollection(dbSeq, Request("https"), App("geovisio.fr"))

	assert res['type'] == "Collection"
	assert res['stac_version'] == "1.0.0"
	assert res['id'] == '12345678-1234-5678-1234-567812345678'
	assert res['title'] == "Test sequence"
	assert res['description'] == "A sequence of geolocated pictures"
	assert res['keywords'] == ["pictures", "Test sequence"]
	assert res['license'] == "proprietary"
	assert res['extent']['spatial']['bbox'] == [[-1.0,-2.0,1.0,2.0]]
	assert res['extent']['temporal']['interval'] == [["2020-01-01T12:50:37", "2020-01-01T13:30:42"]]
	assert len(res['links']) == 4


def test_collectionsEmpty(client):
	response = client.get('/api/collections')
	data = json.loads(response.get_data(as_text=True))

	assert response.status_code == 200
	assert len(data['collections']) == 0
	assert len(data['links']) == 2


@conftest.SEQ_IMGS
def test_collections(datafiles, initSequence):
	client = initSequence(datafiles, preprocess=False)

	response = client.get('/api/collections')
	data = json.loads(response.get_data(as_text=True))

	assert response.status_code == 200

	assert len(data['collections']) == 1
	assert len(data['links']) == 2

	print(data['collections'][0])
	clc = Collection.from_dict(data['collections'][0])

	assert data['collections'][0]['type'] == "Collection"
	assert data['collections'][0]['stac_version'] == "1.0.0"
	assert len(data['collections'][0]['id']) > 0
	assert len(data['collections'][0]['title']) > 0
	assert data['collections'][0]['description'] == "A sequence of geolocated pictures"
	assert len(data['collections'][0]['keywords']) > 0
	assert len(data['collections'][0]['license']) > 0
	assert len(data['collections'][0]['extent']['spatial']['bbox'][0]) == 4
	assert len(data['collections'][0]['extent']['temporal']['interval'][0]) == 2
	assert len(data['collections'][0]['links']) == 4


def test_collectionMissing(client):
	response = client.get('/api/collections/00000000-0000-0000-0000-000000000000')
	assert response.status_code == 404


@conftest.SEQ_IMGS
def test_collectionById(datafiles, initSequence, dburl):
	client = initSequence(datafiles, preprocess=False)

	with psycopg.connect(dburl) as conn:
		with conn.cursor() as cursor:
			seqId = cursor.execute("SELECT id FROM sequences LIMIT 1").fetchone()[0]

			response = client.get('/api/collections/'+str(seqId))
			data = json.loads(response.get_data(as_text=True))

			assert response.status_code == 200
			clc = Collection.from_dict(data)


@conftest.SEQ_IMGS
def test_items(datafiles, initSequence, dburl):
	client = initSequence(datafiles, preprocess=False)

	with psycopg.connect(dburl) as conn:
		with conn.cursor() as cursor:
			seqId = cursor.execute("SELECT id FROM sequences LIMIT 1").fetchone()[0]

			response = client.get('/api/collections/' + str(seqId) + '/items')
			data = json.loads(response.get_data(as_text=True))

			assert response.status_code == 200

			assert data['type'] == "FeatureCollection"
			assert len(data['features']) == 5
			assert len(data['links']) == 1

			clc = ItemCollection.from_dict(data)
			assert len(clc) == 5

			# Check if items have next/prev picture info
			i = 0
			for item in clc:
				nbPrev = len([ l for l in item.links if l.rel == "prev" ])
				nbNext = len([ l for l in item.links if l.rel == "next" ])
				if i == 0:
					assert nbPrev == 0
					assert nbNext == 1
				elif i == len(clc) - 1:
					assert nbPrev == 1
					assert nbNext == 0
				else:
					assert nbPrev == 1
					assert nbNext == 1

				i += 1

			# Make one picture not available
			picHidden = data['features'][0]['id']
			cursor.execute("UPDATE pictures SET status = 'hidden' WHERE id = %s", [picHidden])
			conn.commit()

			response = client.get('/api/collections/' + str(seqId) + '/items')
			data = json.loads(response.get_data(as_text=True))

			assert response.status_code == 200

			assert data['type'] == "FeatureCollection"
			assert len(data['features']) == 4
			picIds = [ f['id'] for f in data['features'] ]
			assert picHidden not in picIds


@conftest.SEQ_IMGS
def test_item(datafiles, initSequence, dburl):
	client = initSequence(datafiles, preprocess=False)

	with psycopg.connect(dburl) as conn:
		with conn.cursor() as cursor:
			seqId, picId = cursor.execute("SELECT seq_id, pic_id FROM sequences_pictures WHERE rank = 1").fetchone()

			response = client.get('/api/collections/' + str(seqId) + '/items/' + str(picId))
			data = json.loads(response.get_data(as_text=True))

			assert response.status_code == 200

			assert data['type'] == "Feature"
			assert data['geometry']['type'] == 'Point'
			assert len(str(data['id'])) > 0
			assert re.match(r'^2021-07-29T', data['properties']['datetime'])
			assert data['properties']['view:azimuth'] >= 0
			assert data['properties']['view:azimuth'] <= 360
			assert re.match('^https?://.*/api/pictures/'+str(picId)+'/tiled/\{TileCol\}_\{TileRow\}.jpg$', data['asset_templates']['tiles']['href'])
			assert re.match('^https?://.*/api/pictures/'+str(picId)+'/tiled/\{TileCol\}_\{TileRow\}.webp$', data['asset_templates']['tiles_webp']['href'])
			assert re.match('^https?://.*/api/pictures/'+str(picId)+'/hd.jpg$', data['assets']['hd']['href'])
			assert re.match('^https?://.*/api/pictures/'+str(picId)+'/sd.jpg$', data['assets']['sd']['href'])
			assert re.match('^https?://.*/api/pictures/'+str(picId)+'/hd.webp$', data['assets']['hd_webp']['href'])
			assert re.match('^https?://.*/api/pictures/'+str(picId)+'/sd.webp$', data['assets']['sd_webp']['href'])
			assert re.match('^https?://.*/api/pictures/'+str(picId)+'/thumb.jpg$', data['assets']['thumb']['href'])
			assert re.match('^https?://.*/api/pictures/'+str(picId)+'/thumb.webp$', data['assets']['thumb_webp']['href'])
			assert data['properties']['tiles:tile_matrix_sets']['geovisio']['tileMatrix'][0]['tileWidth'] == 720
			assert data['properties']['tiles:tile_matrix_sets']['geovisio']['tileMatrix'][0]['tileHeight'] == 720
			assert data['properties']['tiles:tile_matrix_sets']['geovisio']['tileMatrix'][0]['matrixHeight'] == 4
			assert data['properties']['tiles:tile_matrix_sets']['geovisio']['tileMatrix'][0]['matrixWidth'] == 8
			assert data['properties']['pers:interior_orientation']['camera_manufacturer'] == "GoPro"
			assert data['properties']['pers:interior_orientation']['camera_model'] == "Max"
			assert data['properties']['pers:interior_orientation']['field_of_view'] == 360

			item = Item.from_dict(data)
			assert len(item.links) == 3
			assert len([ l for l in item.links if l.rel == "next"]) == 1

			# Make picture not available
			cursor.execute("UPDATE pictures SET status = 'hidden' WHERE id = %s", [picId])
			conn.commit()

			response = client.get('/api/collections/' + str(seqId) + '/items/' + str(picId))
			assert response.status_code == 404


@conftest.SEQ_IMGS_FLAT
def test_item_flat(datafiles, initSequence, dburl):
	client = initSequence(datafiles, preprocess=False)

	with psycopg.connect(dburl) as conn:
		with conn.cursor() as cursor:
			seqId, picId = cursor.execute("SELECT seq_id, pic_id FROM sequences_pictures WHERE rank = 1").fetchone()

			response = client.get('/api/collections/' + str(seqId) + '/items/' + str(picId))
			data = json.loads(response.get_data(as_text=True))

			assert response.status_code == 200

			assert data['type'] == "Feature"
			assert data['geometry']['type'] == 'Point'
			assert len(str(data['id'])) > 0
			assert re.match(r'^2015-04-25T', data['properties']['datetime'])
			assert data['properties']['view:azimuth'] >= 0
			assert data['properties']['view:azimuth'] <= 360
			assert re.match('^https?://.*/api/pictures/'+str(picId)+'/hd.jpg$', data['assets']['hd']['href'])
			assert re.match('^https?://.*/api/pictures/'+str(picId)+'/sd.jpg$', data['assets']['sd']['href'])
			assert re.match('^https?://.*/api/pictures/'+str(picId)+'/hd.webp$', data['assets']['hd_webp']['href'])
			assert re.match('^https?://.*/api/pictures/'+str(picId)+'/sd.webp$', data['assets']['sd_webp']['href'])
			assert re.match('^https?://.*/api/pictures/'+str(picId)+'/thumb.jpg$', data['assets']['thumb']['href'])
			assert re.match('^https?://.*/api/pictures/'+str(picId)+'/thumb.webp$', data['assets']['thumb_webp']['href'])
			assert 'assert_templates' not in data
			assert 'tiles:tile_matrix_sets' not in data['properties']
			assert data['properties']['pers:interior_orientation']['camera_manufacturer'] == "OLYMPUS IMAGING CORP."
			assert data['properties']['pers:interior_orientation']['camera_model'] == "SP-720UZ"
			assert data['properties']['pers:interior_orientation']['field_of_view'] is None # Not in cameras DB

			item = Item.from_dict(data)
			assert len(item.links) == 3
			assert len([ l for l in item.links if l.rel == "next"]) == 1


@conftest.SEQ_IMG_FLAT
def test_item_flat_fov(datafiles, initSequence, dburl):
	client = initSequence(datafiles, preprocess=False)

	with psycopg.connect(dburl) as conn:
		with conn.cursor() as cursor:
			seqId, picId = cursor.execute("SELECT seq_id, pic_id FROM sequences_pictures WHERE rank = 1").fetchone()

			response = client.get('/api/collections/' + str(seqId) + '/items/' + str(picId))
			data = json.loads(response.get_data(as_text=True))

			assert response.status_code == 200

			assert len(str(data['id'])) > 0
			assert data['properties']['pers:interior_orientation']['camera_manufacturer'] == "Canon"
			assert data['properties']['pers:interior_orientation']['camera_model'] == "EOS 6D0"
			assert data['properties']['pers:interior_orientation']['field_of_view'] is None # Not in cameras DB


intersectsGeojson1 = json.dumps({ "type": "Polygon", "coordinates": [[
	[ 1.9191969931125639, 49.00691313179996 ],
	[ 1.9191332906484602, 49.00689685694783 ],
	[ 1.9191691651940344, 49.00687024535389 ],
	[ 1.919211409986019, 49.006892018477274 ],
	[ 1.9191969931125639, 49.00691313179996 ]
]]})
intersectsGeojson2 = json.dumps({ "type": "Point", "coordinates": [ 1.919185442, 49.00688962 ] })

@pytest.mark.parametrize(
	('limit', 'bbox', 'datetime', 'intersects', 'ids', 'collections', 'httpCode', 'validRanks'), (
	(   None,   None,       None,         None,  None,          None,        200,  [1,2,3,4,5]),
	(      2,   None,       None,         None,  None,          None,        200,         None),
	(     -1,   None,       None,         None,  None,          None,        400,         None),
	(  99999,   None,       None,         None,  None,          None,        400,         None),
	(  "bla",   None,       None,         None,  None,          None,        400,         None),
	(   None, [0,0,1,1],    None,         None,  None,          None,        200,           []),
	(   None, "[0,0,1,1",   None,         None,  None,          None,        400,         None),
	(   None,    [1],       None,         None,  None,          None,        400,         None),
	(None, [1.919185,49.00688,1.919187,49.00690], None, None,  None, None,   200,          [1]),
	(   None,   None, "2021-07-29T11:16:54+02", None, None,     None,        200,          [1]),
	(   None,   None, "2021-07-29T00:00:00Z/..", None, None,    None,        200,  [1,2,3,4,5]),
	(   None,   None, "../2021-07-29T00:00:00Z", None, None,    None,        200,           []),
	(   None,   None, "2021-01-01T00:00:00Z/2021-07-29T11:16:58+02", None, None, None, 200, [1,2,3]),
	(   None,   None, "2021-01-01T00:00:00Z/", None, None,      None,        400,         None),
	(   None,   None, "/2021-01-01T00:00:00Z", None, None,      None,        400,         None),
	(   None,   None,       "..",         None,  None,          None,        400,         None),
	(   None,   None, "2021-07-29TNOTATIME", None, None,        None,        400,         None),
	(   None,   None,       None, intersectsGeojson1,  None,    None,        200,        [1,2]),
	(   None,   None,       None, intersectsGeojson2,  None,    None,        200,          [1]),
	(   None,   None,       None, "{ 'broken': ''",  None,      None,        400,         None),
	(   None,   None,       None, "{ 'type': 'Feature' }",  None, None,      400,         None),
	(   None,   None,       None,         None, [1,2],          None,        200,        [1,2]),
	(   None,   None,       None,         None,  None,          True,        200,  [1,2,3,4,5]),
))
@conftest.SEQ_IMGS
def test_search(datafiles, initSequence, dburl, limit, bbox, datetime, intersects, ids, collections, httpCode, validRanks):
	client = initSequence(datafiles, preprocess=False)

	# Transform input ranks into picture ID to pass to query
	if ids is not None and len(ids) > 0:
		with psycopg.connect(dburl) as conn:
			with conn.cursor() as cursor:
				ids = json.dumps(cursor.execute("SELECT array_to_json(array_agg(pic_id::varchar)) FROM sequences_pictures WHERE rank = ANY(%s)", [ids]).fetchone()[0])

	# Retrieve sequence ID to pass into collections in query
	if collections is True:
		with psycopg.connect(dburl) as conn:
			with conn.cursor() as cursor:
				collections = json.dumps([ cursor.execute("SELECT id::varchar FROM sequences").fetchone()[0] ])

	query = {"limit": limit, "bbox": bbox, "datetime": datetime, "intersects": intersects, "ids": ids, "collections": collections}
	query = dict(filter(lambda val: val[1] is not None,query.items()))

	response = client.get('/api/search?'+urlencode(query))
	data = json.loads(response.get_data(as_text=True))

	# ~ print(data)
	assert response.status_code == httpCode

	if httpCode == 200:
		clc = ItemCollection.from_dict(data)

		if validRanks is not None:
			assert len(clc) == len(validRanks)

			if len(validRanks) > 0:
				with psycopg.connect(dburl) as db:
					validIds = db.execute("SELECT array_agg(pic_id ORDER BY rank) FROM sequences_pictures WHERE rank = ANY(%s)", [validRanks]).fetchone()[0]
					allIds = db.execute("SELECT array_agg(pic_id ORDER BY rank) FROM sequences_pictures").fetchone()[0]
					resIds = [ UUID(item.id) for item in clc ]
					print(allIds)
					print(validIds)
					assert sorted(resIds) == sorted(validIds)

					for i in range(len(validRanks)):
						r = validRanks[i]
						id = validIds[i]
						links = [ it.links for it in clc.items if it.id == str(id) ][0]
						if r == 1:
							assert [ l.extra_fields['id'] for l in links if l.rel == "next"] == [str(allIds[r])]
							assert [ l.extra_fields['id'] for l in links if l.rel == "prev"] == []
						elif r == 5:
							assert [ l.extra_fields['id'] for l in links if l.rel == "next"] == []
							assert [ l.extra_fields['id'] for l in links if l.rel == "prev"] == [str(allIds[r-2])]
						else:
							assert [ l.extra_fields['id'] for l in links if l.rel == "next"] == [str(allIds[r])]
							assert [ l.extra_fields['id'] for l in links if l.rel == "prev"] == [str(allIds[r-2])]

		elif limit is not None:
			assert len(clc) == limit
