import psycopg
from src import db_migrations, create_app
import pytest
import psycopg

def test_upgrade_downgrade_upgrade(dburl, app):
	"""Tests that schema upgrade -> downgrade -> upgrade"""
	# At startup the database should have an up to date schema 
	assert _pictures_table_exists(dburl)
 
	# downgrade the schema
	db_migrations.rollback_db_schema(dburl, rollback_all=True)

	# after the downgrade there should not be a pictures table anymore
	assert not _pictures_table_exists(dburl)
 
	# if we apply the schema again we get back the table
	db_migrations.update_db_schema(dburl, force=True)
	assert _pictures_table_exists(dburl)
 
def test_one_rollback(dburl):
	"""Creating an app with an invalid database lead to an error"""
	db_migrations.update_db_schema(dburl, force=True)
	assert _pictures_table_exists(dburl)
	backend = db_migrations.get_yoyo_backend(dburl)
	initial_migrations_applyed = backend.get_applied_migration_hashes()
	assert len(initial_migrations_applyed) > 0
 
	# downgrade the schema
	db_migrations.rollback_db_schema(dburl, rollback_all=False)
 
	# we should have one less migration
	migrations_applyed = backend.get_applied_migration_hashes()
	assert len(migrations_applyed) == len(initial_migrations_applyed) - 1
 
def test_init_bad_db():
	"""Creating an app with an invalid database lead to an error"""
	invalidUrl = "postgres://postgres@invalid_host/geovisio"
	with pytest.raises(psycopg.OperationalError):
		create_app({ 'TESTING': True, 'DB_URL': invalidUrl})

def _pictures_table_exists(dburl):
		with psycopg.connect(dburl) as conn:
			with conn.cursor() as cursor:
				return cursor.execute("SELECT EXISTS(SELECT relname FROM pg_class WHERE relname = 'pictures')").fetchone()[0]
